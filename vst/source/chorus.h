#ifndef __again__
#define __again__

#include "audioeffectx.h"
#include "Parameter.h"
#include <math.h>

//-------------------------------------------------------------------------------------------------------
class Chorus : public AudioEffectX
{
public:
	Chorus (audioMasterCallback audioMaster);
	~Chorus ();

	// Processingz
	virtual void processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames);

	// Program
	virtual void setProgramName (char* name);
	virtual void getProgramName (char* name);

	// Parameters
	virtual void setParameter (VstInt32 index, float value);
	virtual float getParameter (VstInt32 index);
	virtual void getParameterLabel (VstInt32 index, char* label);
	virtual void getParameterDisplay (VstInt32 index, char* text);
	virtual void getParameterName (VstInt32 index, char* text);

	virtual bool getEffectName (char* name);
	virtual bool getVendorString (char* text);
	virtual bool getProductString (char* text);
	virtual VstInt32 getVendorVersion ();

private:
	void _initParameters();

	float _lfo(float amplitude);

protected:
	ParameterList mParameters;
	char programName[kVstMaxProgNameLen + 1];
	//time between original voice and second voice, in ms
	unsigned short int mTimeOffset;

	float* mDelayBuffer;
	size_t mDelayCursor;
	size_t mDelayBufferSize;

	size_t mLFOCursor;
	float mLFOHz;
	
	///Sample rate of the audio we're acting upon.
	double mSampleRate;
};

#endif
