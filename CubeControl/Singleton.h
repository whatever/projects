#ifndef _CUBECONTROL_SINGLETON_H_
#define _CUBECONTROL_SINGLETON_H_
#pragma once

namespace CubeControl
{
	template<class T>
	class Singleton
	{
	public:
		Singleton()
		{
			mSingleton = static_cast<T*>(this);
		}

		static T* getSingletonPtr()
		{
			if(!mSingleton)
				throw new std::string("Using singleton which isn't initialized.");
			return mSingleton;
		}

	private:
		static T* mSingleton;
	}; // end class Singleton

	template <class T> T* Singleton<T>::mSingleton = NULL;
};

#endif
