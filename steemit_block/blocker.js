var hideNegative = true;
var usersToBlock = [];

function init()
{
	chrome.storage.sync.get({
		// defaults
		usersToBlock: "",
		hideNegative: false
	}, function(items) {
		hideNegative = items.hideNegative;
		usersToBlock = items.usersToBlock.split(/,\s*/);
		
		removeBlockedComments();
		setInterval(removeBlockedComments,500);
	});
}
	

function removeElement(element)
{
	element.style.display='none !important';
		
	if( element.parentNode != null )
	{
		element.parentNode.removeChild(element);
	}
}

function removeBlockedComments()
{
	if( hideNegative )
	{
		var downvoted = document.querySelectorAll('div.downvoted');
		for(var i = 0; i < downvoted.length; ++i)
		{
			if( downvoted[i].parentNode != null )
				removeElement(downvoted[i].parentNode);
			else
				removeElement(downvoted[i]);
		}
	}
	
	if( usersToBlock.length > 0 )
	{
		for( var i = 0; i < usersToBlock.length; ++i )
		{
			if( usersToBlock[i].trim().length > 0 )
			{
				var xpathResult = document.evaluate('//*[text()="' + usersToBlock[i] + '"]/ancestor::div[contains(concat(" ", @class, " "), "PostSummary__content") or contains(concat(" ", @class, " "), "hentry")][1]', document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
				
				for (var j=0; j<xpathResult.snapshotLength; j++)
				{
					removeElement(xpathResult.snapshotItem(j));
				}
			}
		}
	}
}

init();