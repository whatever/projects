function saveOptions()
{
	var block = document.getElementById("usersToBlock").value;
	var negative = document.getElementById("hideNegative").checked;
	
	var status = document.getElementById('status');
	
	chrome.storage.sync.set({
		usersToBlock: block,
		hideNegative: negative
	}, function() {
		
		status.textContent = 'Options saved.';
		setTimeout(function() {
		  status.textContent = '';
		}, 2000);
	});
}

function onDOMLoaded()
{
	restoreOptions();
	
	document.getElementById('save').addEventListener('click', saveOptions);
}

function restoreOptions()
{
	chrome.storage.sync.get({
		// defaults
		usersToBlock: "",
		hideNegative: false
	}, function(items) {
		document.getElementById("usersToBlock").value = items.usersToBlock;
		document.getElementById("hideNegative").checked = items.hideNegative;
	});
}

document.addEventListener('DOMContentLoaded', onDOMLoaded);