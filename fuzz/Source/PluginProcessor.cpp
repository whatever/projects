#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
EuphonicFuzzAudioProcessor::EuphonicFuzzAudioProcessor()
{
}

EuphonicFuzzAudioProcessor::~EuphonicFuzzAudioProcessor()
{
}

//==============================================================================
const String EuphonicFuzzAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

int EuphonicFuzzAudioProcessor::getNumParameters()
{
    return 0;
}

float EuphonicFuzzAudioProcessor::getParameter (int index)
{
    switch(index)
	{
	case pregainParam:
		return mPregain;
	case outputParam:
		return mOutput;
	case drive1Param:
		return mDrive1;
	case drive2Param:
		return mDrive2;
	case drive3Param:
		return mDrive3;
	default:
		return 0;
	}
	return 0.0f;
}

void EuphonicFuzzAudioProcessor::setParameter (int index, float newValue)
{
	switch(index)
	{
	case pregainParam:
		mPregain = newValue;
		break;
	case outputParam:
		mOutput = newValue;
		break;
	case drive1Param:
		mDrive1 = newValue;
		break;
	case drive2Param:
		mDrive2 = newValue;
		break;
	case drive3Param:
		mDrive3 = newValue;
		break;
	}
}

const String EuphonicFuzzAudioProcessor::getParameterName (int index)
{
	switch(index)
	{
	case pregainParam:
		return "pregain";
	case outputParam:
		return "output";
	case drive1Param:
		return "drive1";
	case drive2Param:
		return "drive2";
	case drive3Param:
		return "drive3";
	default:
		return "";
	}
	return "";
}

const String EuphonicFuzzAudioProcessor::getParameterText (int index)
{
    return String (getParameter (index), 2);
}

const String EuphonicFuzzAudioProcessor::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String EuphonicFuzzAudioProcessor::getOutputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

bool EuphonicFuzzAudioProcessor::isInputChannelStereoPair (int index) const
{
    return true;
}

bool EuphonicFuzzAudioProcessor::isOutputChannelStereoPair (int index) const
{
    return true;
}

bool EuphonicFuzzAudioProcessor::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool EuphonicFuzzAudioProcessor::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

int EuphonicFuzzAudioProcessor::getNumPrograms()
{
    return 0;
}

int EuphonicFuzzAudioProcessor::getCurrentProgram()
{
    return 0;
}

void EuphonicFuzzAudioProcessor::setCurrentProgram (int index)
{
}

const String EuphonicFuzzAudioProcessor::getProgramName (int index)
{
    return String::empty;
}

void EuphonicFuzzAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void EuphonicFuzzAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void EuphonicFuzzAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void EuphonicFuzzAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    const int numSamples = buffer.getNumSamples();

	for (int channel = 0; channel < getNumInputChannels(); ++channel)
    {
        float* channelData = buffer.getSampleData (channel);

		for (int i = 0; i < numSamples; ++i)
		{
			const float in = channelData[i] * mPregain;

			channelData[i] = _drive1(in) * mDrive1 +
				_drive2(in) * mDrive2 +
				_drive3(in) * mDrive3;

			channelData[i] *= mOutput;
		}
    }

    // In case we have more outputs than inputs, we'll clear any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = getNumInputChannels(); i < getNumOutputChannels(); ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
    }
}

float EuphonicFuzzAudioProcessor::_drive1(float in)
{
	// this function is not defined below zero, so flip the signal and flip it back
	// after processing if necessary
	bool flipped = false;
	if(in < 0)
	{
		in *= -1;
		flipped = true;
	}
	
	float out = pow(in * 0.5f, 1.f/11.f) * 0.7f;
	
	if(flipped)
		out *= -1;

	return out;
}

float EuphonicFuzzAudioProcessor::_drive2(float in)
{
	return tanh(in * 1.5f);
}

float EuphonicFuzzAudioProcessor::_drive3(float in)
{
	bool flipped = false;
	if(in < 0)
	{
		in *= -1;
		flipped = true;
	}

	float out = pow(in * 0.5f, 1.f/5.f) * 0.7f;

	if(flipped)
		out *= -1;

	return out;
}

//==============================================================================
bool EuphonicFuzzAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* EuphonicFuzzAudioProcessor::createEditor()
{
    return new EuphonicFuzzAudioProcessorEditor (this);
}

//==============================================================================
void EuphonicFuzzAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void EuphonicFuzzAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new EuphonicFuzzAudioProcessor();
}
