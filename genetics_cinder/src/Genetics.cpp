#include "Genetics.h"

#include "Util.h"

#include <time.h>
#include <algorithm>

namespace Genetics
{
	Individual::Individual(size_t numChromosomes, size_t chromosomeSize)
	{
		_createRandomGenome(numChromosomes, chromosomeSize);
	}

	size_t Individual::getNumChromosomes()
	{
		return mChromosomes.size();
	}

	size_t Individual::getChromosomeSize()
	{
		if(getNumChromosomes() == 0)
			return 0; // There is no genetic material

		// All chromosomes should be of the same size.
		return mChromosomes.back()->size();
	}

	void Individual::mutate(float percentage)
	{
		for(size_t c = 0; c < getNumChromosomes(); ++c)
		{
			mutateChromosome(c, percentage);
		}
	}

	void Individual::mutateChromosome(size_t chromosomeNum, float percentage)
	{
		const size_t CHROMOSOME_SIZE = getChromosomeSize();
		
		assert(chromosomeNum < getNumChromosomes());

		// Randomly choose some indices to change, according to the percentage requested.
		std::vector<size_t> indicesToMutate = _getRandomIndices(percentage * CHROMOSOME_SIZE, CHROMOSOME_SIZE);

		// Randomize the data at those indices.
		Chromosome* chromosome = mChromosomes.at(chromosomeNum);
		for(std::vector<size_t>::const_iterator iter = indicesToMutate.begin(); iter != indicesToMutate.end(); ++iter)
			chromosome->at(*iter) = (char)(rand() % 256);
	}
	
	void Individual::_createRandomGenome(size_t numChromosomes, size_t chromosomeSize)
	{
		srand((unsigned int)time(NULL));

		for(size_t c = 0; c < numChromosomes; ++c) // add chromosomes
		{
			Chromosome* chromosome = new Chromosome();
			mChromosomes.push_back(chromosome);
			
			for(size_t b = 0; b < chromosomeSize; ++b) // add bytes to EACH chromosome
				chromosome->push_back(rand() % 256);
		}
	}

	Individual* Individual::_defaultBreed(Individual* otherParent, float mutationRate)
	{
		// Ensure that the two parents are of the same species
		assert(getChromosomeSize() == otherParent->getChromosomeSize());
		assert(getNumChromosomes() == otherParent->getNumChromosomes());

		Individual* child = new Individual(getNumChromosomes(), getChromosomeSize());

		child->

		if(mutationRate > 0)
			child->mutate(mutationRate);

		return child;
	}

	Individual::Chromosome* Individual::_recombinate(Individual::Chromosome* chromosome1, Individual::Chromosome* chromosome2)
	{
		return NULL;
	}

	std::vector<size_t> Individual::_getRandomIndices( size_t numIndices, size_t arraySize ) const
	{
		std::vector<size_t> indices;

		// TODO: There is probably a better way to generate a list of all possible indices
		for(size_t i = 0; i < arraySize; ++i)
			indices.push_back(i);

		std::random_shuffle(indices.begin(), indices.end());

		return std::vector<size_t>(indices.begin(), indices.begin() + numIndices);
	}

	Population::Population(FitnessFunction fitnessFunction, size_t populationSize, float generalMutationRate,
		size_t chromosomeSize, size_t numChromosomes,
		bool useCataclysmicMutation, IndividualList seedPopulation, BreedingFunction breedingFunction)

		: mFitnessFunction(fitnessFunction), mPopulationSize(populationSize), mGeneralMutationRate(generalMutationRate),
			mChromosomeSize(chromosomeSize), mNumChromosomes(numChromosomes), mBreedingFunction(breedingFunction)
	{
		mIndividuals.insert(seedPopulation.end(), seedPopulation.begin(), seedPopulation.end());

		_fillPopulation();
	}

	Population::~Population()
	{
		for(IndividualList::iterator iter = mIndividuals.begin(); iter != mIndividuals.end(); ++iter)
		{
			SAFE_DELETE(*iter);
		}

		mIndividuals.clear();
	}

	void Population::_fillPopulation()
	{
		while(mIndividuals.size() < mPopulationSize)
		{
			Individual* individual = new Individual(mNumChromosomes, mChromosomeSize);
			mIndividuals.push_back(individual);
		}
	}

	
}