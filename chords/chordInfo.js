/* Class that describes a chord's root, type (quality), and image filename. */
function ChordInfo(imageFilename)
{
	this.imageFilename = imageFilename;
	
	var noteNameLength = 1;
	if(imageFilename[1] == "b") // "flat" designation
	{
		noteNameLength = 2;
	}
	
	this.rootNote = imageFilename.substr(0, noteNameLength);
	
	// e.g. dim9
	this.quality = imageFilename.split(".")[0].substr(noteNameLength);
	if(this.quality.length == 0)
	{
		this.quality = "maj";
	}
	
	this.chordName = this.rootNote + this.quality;
	
	
	
	var self = this;
	this.Log = function()
	{
		console.log(self.imageFilename);
		console.log(self.rootNote);
		console.log(self.quality);
	}
}