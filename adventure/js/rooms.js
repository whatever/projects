$(document).ready(function()
{
	$("#roomsArea").draggable();
});

addRoom = function()
{
	var newRoom = document.createElement("div");
	newRoom.setAttribute("class", "RoomWindow");
	newRoom.setAttribute("id", "room" + allocateRoomID());
	
	$(newRoom).draggable({
		snap:".RoomWindow",
	});
	$(newRoom).editableText();
	
	$(newRoom).html("Enter room description here.");
	
	var roomsArea = document.getElementById("roomsArea");
	roomsArea.appendChild(newRoom);
}

var lastRoomID = 0;
allocateRoomID = function()
{
	return lastRoomID++;
}

startAddDoor = function()
{
	$message = $("#message");
	$message.text("Click two rooms in sequence to create a door between them.");
	$message.css("display", "block");
}