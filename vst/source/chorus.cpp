#include <stdlib.h>
#include "chorus.h"
#include <string>
#include <sstream>

const double PI = 3.14159265;

//-------------------------------------------------------------------------------------------------------
AudioEffect* createEffectInstance (audioMasterCallback audioMaster)
{
	return new Chorus (audioMaster);
}

//-------------------------------------------------------------------------------------------------------
Chorus::Chorus (audioMasterCallback audioMaster)
: AudioEffectX (audioMaster, 1, 2)	// 1 program, 1 parameter only
{
	_initParameters();

	setNumInputs (1);		// mono in
	setNumOutputs (1);		// mono out
	setUniqueID (123456);	// identify
	canProcessReplacing ();	// supports replacing output

	//find out our sample rate.
	VstTimeInfo* info = getTimeInfo(-1);
	mSampleRate = info->sampleRate;

	vst_strncpy (programName, "Default", kVstMaxProgNameLen);	// default program name

	mTimeOffset = 15;

	mDelayBufferSize = size_t((mTimeOffset/1000.0) * mSampleRate * sizeof(float));
	mDelayBuffer = (float*)malloc(mDelayBufferSize);
	memset(mDelayBuffer, 0, mDelayBufferSize);
	mDelayCursor = 0;
	mLFOCursor = 0;
	mLFOHz = 0.45f;
}

//-------------------------------------------------------------------------------------------------------
Chorus::~Chorus ()
{
	//free(mDelayBuffer);
}

void Chorus::_initParameters()
{
	Parameter time1;
	time1.name = "Separat1";
	time1.units = "ms";
	mParameters.push_back(time1);

	Parameter lfo1;
	lfo1.name = "LFO1Freq";
	lfo1.units = "Hz";
	mParameters.push_back(lfo1);
}

//-------------------------------------------------------------------------------------------------------
void Chorus::setProgramName (char* name)
{
	vst_strncpy (programName, name, kVstMaxProgNameLen);
}

//-----------------------------------------------------------------------------------------
void Chorus::getProgramName (char* name)
{
	vst_strncpy (name, programName, kVstMaxProgNameLen);
}

//-----------------------------------------------------------------------------------------
void Chorus::setParameter (VstInt32 index, float value)
{
	switch(index)
	{
	case 0:
		mTimeOffset = (unsigned short int)(value * 50);
		mDelayBufferSize = size_t((mTimeOffset/1000.0) * mSampleRate * sizeof(float));
		if(mDelayBufferSize == 0)
			mDelayBufferSize = 1;
		realloc(mDelayBuffer, mDelayBufferSize);
		memset(mDelayBuffer, 0, mDelayBufferSize);
		mDelayCursor = 0;
		break;
	case 1:
		mLFOHz = value * 5.0f;
		break;
	}
}

//-----------------------------------------------------------------------------------------
float Chorus::getParameter (VstInt32 index)
{
	switch(index)
	{
	case 0:
		return mTimeOffset / 50.0f;
	case 1:
		return mLFOHz / 5.0f;
	default:
		return 0;
	}
}

//-----------------------------------------------------------------------------------------
void Chorus::getParameterName (VstInt32 index, char* label)
{
	strcpy(label, mParameters[index].name.c_str());
}

//-----------------------------------------------------------------------------------------
void Chorus::getParameterDisplay (VstInt32 index, char* text)
{
	std::stringstream ss;
	
	switch(index)
	{
	case 0:
		ss << mTimeOffset;
		break;
	case 1:
		ss << mLFOHz;
		break;
	}
	
	vst_strncpy(text, ss.str().c_str(), kVstMaxParamStrLen);
}

//-----------------------------------------------------------------------------------------
void Chorus::getParameterLabel (VstInt32 index, char* label)
{
	vst_strncpy (label, mParameters[index].units.c_str(), kVstMaxParamStrLen);
}

//------------------------------------------------------------------------
bool Chorus::getEffectName (char* name)
{
	vst_strncpy (name, "Chorus", kVstMaxEffectNameLen);
	return true;
}

//------------------------------------------------------------------------
bool Chorus::getProductString (char* text)
{
	vst_strncpy (text, "Chorus", kVstMaxProductStrLen);
	return true;
}

//------------------------------------------------------------------------
bool Chorus::getVendorString (char* text)
{
	vst_strncpy (text, "Jake Hart", kVstMaxVendorStrLen);
	return true;
}

//-----------------------------------------------------------------------------------------
VstInt32 Chorus::getVendorVersion ()
{ 
	return 1000; 
}

//-----------------------------------------------------------------------------------------
void Chorus::processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames)
{
    float* in1  =  inputs[0];
    float* out1 = outputs[0];

	while (--sampleFrames >= 0)
    {
		*out1 = (*in1) * 0.5f + mDelayBuffer[mDelayCursor] * 0.5f;

		//done with this position of the delay buffer. overwrite with new value.
		mDelayBuffer[mDelayCursor] = (*in1) * 0.5f + _lfo(*in1) * 0.5f;

		in1++;
		out1++;
		mDelayCursor++;

		if(mDelayCursor >= mDelayBufferSize / sizeof(float))
			mDelayCursor = 0;
    }
}

float Chorus::_lfo(float amplitude)
{
	mLFOCursor++;

	if(mLFOCursor >= mSampleRate)
		mLFOCursor = 0;

	return (float)sin((mLFOCursor / mSampleRate) * 2 * PI * mLFOHz) * amplitude;
}