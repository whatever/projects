#ifndef _GENETICS_H_
#define _GENETICS_H_

#include <functional>
#include <vector>

namespace Genetics
{
	/// An individual with all its genetic data.
	class Individual
	{
	public:
		typedef std::vector<char> Chromosome;
		typedef std::vector<Chromosome*> Genome;

		/// Creates a new individual with a random genome.
		Individual(size_t numChromosomes, size_t chromosomeSize);

		size_t getNumChromosomes();
		/// Get size (in bytes) of a single chromosome
		size_t getChromosomeSize();

		/// Mutates a percentage of a particular chromosome. The indices selected for mutation
		/// will be random.
		/// @param chromosomeNum 0-indexed
		/// @param percentage In the range 0.f to 1.f
		void mutateChromosome(size_t chromosomeNum, float percentage);

		/// Mutates all the individual's chromosomes by a given percentage.
		void mutate(float percentage);

	private:
		void _createRandomGenome(size_t numChromosomes, size_t chromosomeSize);

		/// @return The offspring of the two parents. This individual now has space
		///			allocated on the heap, which the caller must take responsibility
		///			for deallocating.
		Individual* _defaultBreed(Individual* otherParent, float mutationRate);		
		
		Chromosome* _recombinate(Chromosome* chromosome1, Chromosome* chromosome2)

		/// Given an array size, returns a list of random indices into the array.
		std::vector<size_t> _getRandomIndices(size_t numIndices, size_t arraySize) const;

		Genome mChromosomes;
	};

	class Population
	{
	public:
		typedef std::function<float(const Individual&)> FitnessFunction;
		typedef std::function<Individual*(const Individual&, const Individual&)> BreedingFunction;
		typedef std::vector<Individual*> IndividualList;

		/// @param useCataclysmicMutation If true, use CHC (Eshelman).
		Population(FitnessFunction fitnessFunction, size_t populationSize, float generalMutationRate,
			size_t chromosomeSize, size_t numChromosomes,
			bool useCataclysmicMutation = true, IndividualList seedPopulation = IndividualList(),
			BreedingFunction breedingFunction = &_breed);

		virtual ~Population();

	private:
		/// This should be called after unfit individuals have been killed.
		/// It breeds the remaining individuals to create offspring, introducing mutation
		/// if required.
		void _fillPopulation();

		/// Picks a split point, then recombinates the two parent chromosomes
		/// around that point.
		/// @return The caller is responsible for deallocating this new chromosome.
		Individual::Chromosome* _recombinate(Individual::Chromosome* chromosome1, Individual::Chromosome* chromosome2);

		/// Determines the fitness of a given individual (0.f to 1.f inclusive). This is
		/// application-specific, so it's passed in when the population
		/// is created.
		FitnessFunction mFitnessFunction;

		BreedingFunction mBreedingFunction;

		size_t mPopulationSize;
		
		/// Between 0 and 1 inclusive (0-100%). Applied to every new individual
		/// that is bred.
		float mGeneralMutationRate;

		/// All individuals in this population.
		IndividualList mIndividuals;

		/// Size (in bytes) of each chromosome.
		size_t mChromosomeSize;

		/// Number of genes per individual.
		size_t mNumChromosomes;
	};
}

#endif