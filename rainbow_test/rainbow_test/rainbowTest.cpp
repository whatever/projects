#include "RainbowTest.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <strsafe.h>
#include <vector>
#include <sstream>

using namespace CubeControl;

// Event passed from console to thread. Think of it as "shared memory."
CubeEvent* gCubeEvent;

int main()
{
	bool success = createCubeUpdateThread();
	if(!success)
	{
		throw "Failed to create cube update thread.";
		return 1;
	}
	
	char command[256];
	bool keepRunning = true;
	while(keepRunning)
	{
		printf("\ncube> ");
		std::cin.getline(command, 256);

		keepRunning = processConsoleCommand(&command[0]);
	}

	return 0;
}

bool processConsoleCommand(char* command)
{
	char* commandArguments = NULL;
	char* firstWord = strtok_s(command, " ", &commandArguments);

	if(strlen(command) == 0)
	{
		gCubeEvent->type = NEXT_MODE;
		gCubeEvent->argument = 0;
	}
	else if(strcmp(firstWord, "help") == 0)
		printHelp();
	else if(strcmp(firstWord, "color") == 0)
	{
		std::stringstream ss(std::stringstream::in | std::stringstream::out);
		unsigned int r=0, g=0, b=0;
		ss << commandArguments;
		
		ss >> r >> g >> b;

		gCubeEvent->type = SET_COLOR;
		gCubeEvent->color = CubeControl::Color3(r, g, b);
		gCubeEvent->argument = 0;
	}
	else if(strcmp(firstWord, "mode") == 0)
	{
		char* secondWord = strtok_s(NULL, " ", &commandArguments);

		gCubeEvent->type = CHANGE_MODE;
		gCubeEvent->argument = atoi(secondWord);
	}
	else if(strcmp(firstWord, "random") == 0)
	{
		char* secondWord = strtok_s(NULL, " ", &commandArguments);

		gCubeEvent->type = RANDOM;
		gCubeEvent->argument = atoi(secondWord);
	}
	else if(strcmp(firstWord, "speed") == 0)
	{
		char* secondWord = strtok_s(NULL, " ", &commandArguments);

		gCubeEvent->type = SPEED;
		gCubeEvent->argument = atoi(secondWord);
	}
	else if(strcmp(firstWord, "power") == 0)
	{
		char* secondWord = strtok_s(NULL, " ", &commandArguments);

		gCubeEvent->type = POWER;
		gCubeEvent->argument = atoi(secondWord);
	}
	else if(strcmp(firstWord, "port") == 0)
	{
		char* secondWord = strtok_s(NULL, " ", &commandArguments);
		gCubeEvent->type = CHANGE_PORT;
		gCubeEvent->argument = atoi(secondWord);
	}
	else if(strcmp(firstWord, "exit") == 0)
		return false;
	else
	{
		printf("Invalid command.\n");
		printHelp();
	}

	return true;
}

void printHelp()
{
	printf("\n--- Commands ---\n\n");
	printf("<enter>\n\tAdvance to the next mode.\n");
	printf("color <number> <number> <number>\n\tChange to a solid RGB color (values are 0-255)\n");
	printf("mode <number>\n\tChange to the given mode number.\n");
	printf("random <number>\n\tSets seconds between random mode changes. 0 for no randomness.\n");
	printf("speed <number>\n\tSets update speed as a percentage.\n");
	printf("power <0|1>\n\t0 'turns off' the cube; 1 turns it back on.\n");
	printf("port <portname>\n\tChange to a cube on a different serial port (1 for COM1, etc.)\n");
	printf("help\n\tDisplay this help message.\n");
	printf("exit\n\tExits the program.\n");
}

Color3 getAverageScreenColor()
{
	// get the device context of the screen
	static HDC hScreenDC = GetDC(NULL);
	// and a device context to put it in
	static HDC hMemoryDC = CreateCompatibleDC(hScreenDC);

	static const int width = GetDeviceCaps(hScreenDC, HORZRES);
	static const int height = GetDeviceCaps(hScreenDC, VERTRES);

	// maybe worth checking these are positive values
	static HBITMAP hBitmap = CreateCompatibleBitmap(hScreenDC, width, height);

	// get a new bitmap
	static HGDIOBJ hOldBitmap = SelectObject(hMemoryDC, hBitmap);

	BitBlt(hMemoryDC, 0, 0, width, height, hScreenDC, 0, 0, SRCCOPY);

	COLORREF color;
	UINT64 rAccum = 0, gAccum = 0, bAccum = 0;

	for(int x = 0; x < width - 5; x += 10)
		for(int y = 0; y < height - 5; y += 10)
		{
			color = GetPixel(hMemoryDC, x, y);
			rAccum += GetRValue(color);
			gAccum += GetGValue(color);
			bAccum += GetBValue(color);
		}

	rAccum /= (float)width * height / 100;
	gAccum /= (float)width * height / 100;
	bAccum /= (float)width * height / 100;

	// clean up
	//DeleteDC(hMemoryDC);
	//DeleteDC(hScreenDC);

	return Color3((BYTE)rAccum, (BYTE)gAccum, (BYTE)bAccum);
}

bool createCubeUpdateThread()
{
	DWORD   dwThreadID;
	gCubeEvent = new CubeEvent();

	HANDLE updateCubeThread = CreateThread(
		NULL,                   // default security attributes
		0,                      // use default stack size  
		updateCube,				// thread function name
		NULL,					// argument to thread function
		0,                      // use default creation flags
		&dwThreadID);			// returns the thread identifier

	if(!updateCubeThread)
		return false;

	return true;
}

DWORD WINAPI updateCube( LPVOID lpParam )
{
	CubeController controller("COM3");

	srand ((unsigned int) time(NULL));

	unsigned char r=0, g=0, b=255;

	clock_t lastChange, currentTime;
	lastChange = currentTime = clock();

	BYTE secsBetweenModes = 15;
	bool randomizeModes = false;
	
	// expressed as 100 over a percentage (1.f is 100% speed)
	float speedModifier = 1.f;
	bool cubeIsOn = true;
	
	// True when we are currently displaying a static color.
	// Mode is ignored until the next CHANGE_MODE command.
	bool isStaticColor = false;

	BYTE mode = MODE_X;

	for(;;)
	{
		if(gCubeEvent->type != SET_COLOR && gCubeEvent->type != NO_OP)
			isStaticColor = false;

		switch(gCubeEvent->type)
		{
		case CHANGE_MODE:
			mode = gCubeEvent->argument;
			break;
		case NEXT_MODE:
			++mode;
			if(mode >= NUM_MODES)
				mode = 0;
			break;
		case SET_COLOR:
			r = gCubeEvent->color.x;
			g = gCubeEvent->color.y;
			b = gCubeEvent->color.z;
			isStaticColor = true;
			break;
		case RANDOM:
			randomizeModes = !!(gCubeEvent->argument);
			if(randomizeModes)
				secsBetweenModes = gCubeEvent->argument;
			break;
		case SPEED:
			speedModifier = 100.f / (float)(gCubeEvent->argument);
			break;
		case POWER:
			cubeIsOn = !!gCubeEvent->argument;
			controller.clearToColor(0, 0, 0);
			controller.swapBuffers();
			break;
		case CHANGE_PORT:
			{
				int portNum = gCubeEvent->argument;
				controller = CubeController("COM" + portNum);
				break;
			}
		case NO_OP:
		default:
			break;
		}

		gCubeEvent->type = NO_OP;
		gCubeEvent->argument = 0;

		if(!cubeIsOn)
		{
			// sleep to avoid eating up so much CPU
			Sleep(2000);
			continue;
		}

		if(isStaticColor)
		{
			controller.clearToColor(r, g, b);
			controller.swapBuffers();
			continue;
		}

		if(randomizeModes)
		{
			// choose the mode
			currentTime = clock();
			if((currentTime - lastChange) / CLOCKS_PER_SEC > secsBetweenModes)
			{
				lastChange = currentTime;
				mode = rand() % NUM_MODES;
			}
		}

		switch(mode)
		{
		case MODE_X: // ascending "X" shape
			for(size_t y = 0; y < 4; ++y)
			{
				controller.clearToColor(0,0,0);
				for(size_t x = 0; x < 4; ++x)
				{
					controller.setPixel(Point3(x, y, x), Point3(r, g, b));
					controller.setPixel(Point3(x, y, 3-x), Point3(r, g, b));
				}
				controller.swapBuffers();
				Sleep(100 * speedModifier);
			}
			break;

		case MODE_RUBIKS_CUBE:
			controller.drawCube(Point3(0,2,2), 2, Color3(0,0,255));
			controller.drawCube(Point3(0,2,0), 2, Color3(0,255,0));
			controller.drawCube(Point3(2,2,2), 2, Color3(255,0,0));
			controller.drawCube(Point3(2,2,0), 2, Color3(255,255,255));

			controller.drawCube(Point3(0,0,2), 2, Color3(255,0,255));
			controller.drawCube(Point3(0,0,0), 2, Color3(0,255,128));
			controller.drawCube(Point3(2,0,2), 2, Color3(128,128,0));
			controller.drawCube(Point3(2,0,0), 2, Color3(128,0,128));
			
			controller.swapBuffers();
			break;
		case MODE_BOUNCING_CUBE:
			{
				static Point3 cubePos(2, 2, 2);
				static Color3 cubeColor(255,0,0);
				static Point3 movementDirection(0, -1, 0);
				Point3 nextPos = cubePos + movementDirection;

				if(nextPos.x < 0 || nextPos.x > 2 ||
					nextPos.y < 0 || nextPos.y > 2 ||
					nextPos.z < 0 || nextPos.z > 2)
				{
					BYTE random = rand() % 6;
					if(random < 2)
						movementDirection = Point3(1, 0, 0);
					else if(random < 4)
						movementDirection = Point3(0, 1, 0);
					else
						movementDirection = Point3(0, 0, 1);

					if(random % 2)
						movementDirection *= -1;

					cubeColor = Point3(rand() % 256, rand() % 256, rand() % 256);

					controller.clearToColor(0,0,0);
					controller.drawCube(cubePos, 2, cubeColor);
					controller.swapBuffers();

					continue;
				}
				else
				{
					cubePos = nextPos;
					controller.clearToColor(0,0,0);
					controller.drawCube(cubePos, 2, cubeColor);
					controller.swapBuffers();
				}

				Sleep(250 * speedModifier);
				break;
			}

		case MODE_FLASH_COLORS:
			controller.clearToColor(rand() % 256, rand() % 256, rand() %256);
			Sleep(1000 * speedModifier);
			controller.swapBuffers();
			break;
		case MODE_STROBE_LIGHT:
			{
				static bool blank = false;
				if(blank)
					controller.clearToColor(0,0,0);
				else
					controller.clearToColor(255, 255, 255);
				blank = !blank;
				Sleep(50 * speedModifier);
				controller.swapBuffers();
				break;
			}
		case MODE_FADE_COLORS: // inefficient code, sorry
			{
				static Point3 color(0,0,255);
				//amount to change the color by
				static Point3 colorModifier(0,1,0);

				color += colorModifier;
				controller.clearToColor(color.x, color.y, color.z);
				controller.swapBuffers();

				if(color.x * colorModifier.x >= 255 ||
					(color.x == 0 && colorModifier.x == -1))
					colorModifier.x *= -1;

				if(color.y * colorModifier.y >= 255 ||
					(color.y == 0 && colorModifier.y == -1))
					colorModifier.y *= -1;

				if(color.z * colorModifier.z >= 255 ||
					(color.z == 0 && colorModifier.z == -1))
					colorModifier.z *= -1;
				

				BYTE makeNewColorModifier = rand() % 256;
				if(makeNewColorModifier == 0)
				{
					// time to choose a new color modifier.
					colorModifier = Point3(0,0,0);
					// 0-1 if we're changing red, 2-3 if green, 4-5 if blue
					BYTE whichColor = rand() % 6;
					if(whichColor < 2)
						colorModifier.x = 1;
					else if(whichColor < 4)
						colorModifier.y = 1;
					else
						colorModifier.z = 1;

					if(whichColor % 2)
						colorModifier *= -1;
				}

				break;
			}

		case MODE_EDGES:
			{
				Color3 bgColor(rand() % 256, rand() % 256, rand() % 256);
				Color3 edgeColor(255 - bgColor.x, 255 - bgColor.y, 255 - bgColor.z);
				controller.clearToColor(bgColor.x, bgColor.y, bgColor.z);
				
				// draw lines on cube edges
				controller.drawLine(Point3(0,0,3), Point3(0,3,3), edgeColor);
				controller.drawLine(Point3(0,0,3), Point3(3,0,3), edgeColor);
				controller.drawLine(Point3(0,3,3), Point3(3,3,3), edgeColor);
				controller.drawLine(Point3(3,0,3), Point3(3,3,3), edgeColor);

				controller.drawLine(Point3(0,0,0), Point3(0,3,0), edgeColor);
				controller.drawLine(Point3(0,0,0), Point3(3,0,0), edgeColor);
				controller.drawLine(Point3(0,3,0), Point3(3,3,0), edgeColor);
				controller.drawLine(Point3(3,0,0), Point3(3,3,0), edgeColor);

				controller.drawLine(Point3(0,0,0), Point3(0,0,3), edgeColor);
				controller.drawLine(Point3(0,3,0), Point3(0,3,3), edgeColor);

				controller.drawLine(Point3(3,0,0), Point3(3,0,3), edgeColor);
				controller.drawLine(Point3(3,3,0), Point3(3,3,3), edgeColor);

				controller.swapBuffers();

				Sleep(100 * speedModifier);

				break;
			}
		case MODE_COMET:
			{
				static std::vector<Point3> pos(4, Point3(0,0,0));
				static Color3 color(255,0,0);
				static Point3 movementDirection(0, -1, 0);
				Point3 nextPos = pos[0] + movementDirection;

				// if we've struck an edge, pick a new random axis
				// and color.
				if(nextPos.x < 0 || nextPos.x > 3 ||
					nextPos.y < 0 || nextPos.y > 3 ||
					nextPos.z < 0 || nextPos.z > 3)
				{
					BYTE random = rand() % 6;
					if(random < 2)
						movementDirection = Point3(1, 0, 0);
					else if(random < 4)
						movementDirection = Point3(0, 1, 0);
					else
						movementDirection = Point3(0, 0, 1);

					if(random % 2)
						movementDirection *= -1;
				}
				else // did not strike the end of the display
				{
					pos.insert(pos.begin(), 1, nextPos);
					pos.pop_back();
					controller.clearToColor(0,0,0);

					Sleep(100 * speedModifier);
				}

				color.x += rand() % 8;
				color.y += rand() % 8;
				color.z += rand() % 8;

				controller.clearToColor(0,0,0);
				// draw the comet, which is bright at its locus
				// and gets fainter toward the tail
				for(size_t i = 0; i < pos.size(); ++i)
				{
					float3 dimmedHSL = RGB2HSL(color.x, color.y, color.z);
					dimmedHSL.z *= (pos.size() - i) * 0.2f;
					Color3 dimmedRGB = HSL2RGB(dimmedHSL.x, dimmedHSL.y, dimmedHSL.z);
					controller.setPixel(pos[i], dimmedRGB);
				}

				controller.swapBuffers();

				break;
			}
		case RGB_MAGIC:
			{
				static BYTE phase = 0;
				++phase;
				if(phase > 2)
					phase = 0;

				if(phase == 0)
					controller.clearToColor(255, 0, 0);
				else if(phase == 1)
					controller.clearToColor(0, 255, 0);
				else if(phase == 2)
					controller.clearToColor(0, 0, 255);

				controller.swapBuffers();

				break;
			}
		case MODE_WATERFALL:
			{
				static unsigned short int phase = 0;
				phase += 1;
				if(phase > 2)
					phase = 0;
				
				static CubeControl::Point3 col1;
				static CubeControl::Point3 col2;

				if(phase == 0)
				{
					col1 = CubeControl::Point3(255, 0, 0);
					col2 = CubeControl::Point3(0, 255, 0);
				}
				else if(phase == 1)
				{
					col1 = CubeControl::Point3(0, 255, 0);
					col2 = CubeControl::Point3(0, 0, 255);
				}
				else if(phase == 2)
				{
					col1 = CubeControl::Point3(0, 0, 255);
					col2 = CubeControl::Point3(255, 0, 0);
				}

				CubeControl::Point3 pos;
				for(pos.x = 0; pos.x < 4; ++pos.x)
					for(pos.y = 0; pos.y < 4; ++pos.y)
						for(pos.z = 0; pos.z < 4; ++pos.z)
						{
							if((pos.x + pos.y + pos.z) % 2)
								controller.setPixel(pos, col1);
							else
								controller.setPixel(pos, col2);

							controller.swapBuffers();
						}

				controller.swapBuffers();
				break;
			}

		case MODE_RAINBOW:
			{
				static unsigned short int hue = 0;
				hue += 5;

				if(hue >= 360)
					hue = 0;
				Point3 rgb = CubeControl::HSL2RGB(hue / 360.f, 1.f, 0.5);
				controller.clearToColor(rgb.x, rgb.y, rgb.z);
				controller.swapBuffers();
				break;
			}
		default:
			{
				std::cout << "Error: Reached default case." << std::endl;
				break;
			}

		} // end switch(mode)
	}
	
	return 0;
}