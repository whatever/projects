var chordInfos;
var currentChord;
var chordsLoaded = 0;

var context;
var metronome;
var metronomeHigh;
var metronomeTimeoutID;

var circleOfFifths = ['C', 'G', 'D', 'A', 'E', 'B', 'Gb', 'Db', 'Ab', 'Eb', 'Bb', 'F'];

$(document).ready( function()
{
	Center(".Frame");
	
	chordInfos = new Array();
	
	// Load the filenames of all the chords
	$.ajax({
		url : "ukeChords/chordImages.txt",
		dataType: "text",
		success : function (data)
		{
			var $chordContainer = $("#ChordContainer");
			var $chordQualitySelector = $("#startingChordQuality");
		
			$.each( data.split('\n'), function()
			{
				var chordImageName = String(this);
				var chordInfo = new ChordInfo(chordImageName);
				chordInfos.push(chordInfo);
				
				var $chord = $("<div>", {id: chordInfo.chordName, class: "ChordPane"});
				$chord.html("<img src='ukeChords/" + chordInfo.imageFilename + "'>");
				$chordContainer.append($chord);
				$chord.hide();
				
				// Add chord quality to dropdown if not already added
				if( $("#startingChordQuality option[value='" + chordInfo.quality + "']").length == 0 )
				{
					$chordQualitySelector.append("<option value='" + chordInfo.quality + "'>" + chordInfo.quality + "</option>");
				}
				
				++chordsLoaded;
			});
		}
	});
	
	window.AudioContext = window.AudioContext||window.webkitAudioContext;
    context = new AudioContext();
	
	bufferLoader = new BufferLoader(
    context,
    [
      "metronome.wav",
	  "metronome-high.wav"
    ],
    FinishedLoading
    );

	bufferLoader.load();
	
	$(".Numeric").keyup(function () { 
		this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	
	$("#ChordContainer").hide();
});

// Using a jQuery selector, centers something both horizontally and
// vertically within the window.
function Center(selector)
{
	$(window).resize(function()
	{
		$(selector).css({
			position:'absolute',
			left: ($(window).width() - $(selector).outerWidth())/2,
			top: ($(window).height() - $(selector).outerHeight())/2
		});

	});

	$(window).resize();
}

function FinishedLoading(bufferList)
{
  metronome = bufferList[0];
  metronomeHigh = bufferList[1];
}

function StartPractice()
{
	var startingChordQuality = $("#startingChordQuality").val();
	var startingChordRoot = $("#startingChordRoot").val();	
	
	currentChord = Enumerable.From(chordInfos).Where(function(x) { return x.rootNote == startingChordRoot && x.quality == startingChordQuality; }).FirstOrDefault();
	$(".ChordPane").hide();
	$("#" + currentChord.chordName).show();
	
	$("#ChordContainer").show();
	
	TickMetronome();
	
	$("#StartButton").html("Stop");
	$("#StartButton").get(0).onclick = StopPractice;
}

function StopPractice()
{
	$("#StartButton").html("Start");
	clearTimeout(metronomeTimeoutID);
	$("#StartButton").get(0).onclick = StartPractice;
}

var beatsSinceStart = -1;
function TickMetronome()
{
	clearTimeout(metronomeTimeoutID);
	
	++beatsSinceStart;
	
	var barsPerChordChange = parseInt($("#barsPerChordChange").val(), 10);
	
	if( beatsSinceStart % ( barsPerChordChange * 4 ) == 0)
		ShowNextChord();
	
	if( beatsSinceStart % 4 == 0 )
		playSound(metronomeHigh, 0);
	else
		playSound(metronome, 0);
	
	var delay = 60 * 1000 / parseInt($("#bpm").val(), 10);
	metronomeTimeoutID = setTimeout("TickMetronome()", delay);
}

function ShowNextChord()
{
	//var chordName = GetRandomChord().chordName;
	currentChord = GetNextChord(currentChord);
	
	$(".ChordPane").hide();
	$("#" + currentChord.chordName).show();
}

function GetNextChord(currentChord)
{
	var chordProgressionMethod = $("#method").val();
	
	switch( chordProgressionMethod )
	{
	case "Circle of fifths":
	case "Circle of fourths":
		var reverse = (chordProgressionMethod == "Circle of fourths");
		var nextRoot = GetNextInCircleOfFifths(currentChord.rootNote, reverse);
		return Enumerable.From(chordInfos).Where(function(x) { return x.rootNote == nextRoot && x.quality == currentChord.quality; }).FirstOrDefault();
	case "Random":
		var index = Math.floor( Math.random() * chordInfos.length );
		return chordInfos[index];
	default:
		console.log("Invalid method: " + chordProgressionMethod);
		break;
	}
}

function GetNextInCircleOfFifths(note, reverse)
{
	var modifier = reverse ? -1 : 1;
	
	var index = circleOfFifths.indexOf(note) + modifier;
	if( index >= circleOfFifths.length )
		return circleOfFifths[0];
	else if( index < 0 )
		return circleOfFifths[circleOfFifths.length - 1];
	else
		return circleOfFifths[index];
}

function playSound(buffer, time)
{
  var source = context.createBufferSource();
  source.buffer = buffer;
  source.connect(context.destination);
  source.start(time);
}