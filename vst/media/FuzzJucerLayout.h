/*
  ==============================================================================

  This is an automatically generated file created by the Jucer!

  Creation date:  12 Sep 2010 9:59:44pm

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Jucer version: 1.12

  ------------------------------------------------------------------------------

  The Jucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright 2004-6 by Raw Material Software ltd.

  ==============================================================================
*/

#ifndef __JUCER_HEADER_NEWJUCERCOMPONENT_FUZZJUCERLAYOUT_6BBFB5F5__
#define __JUCER_HEADER_NEWJUCERCOMPONENT_FUZZJUCERLAYOUT_6BBFB5F5__

//[Headers]     -- You can add your own extra header files here --
#include "juce.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Jucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class NewJucerComponent  : public Component,
                           public SliderListener
{
public:
    //==============================================================================
    NewJucerComponent ();
    ~NewJucerComponent();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    //[/UserMethods]

    void paint (Graphics& g);
    void resized();
    void sliderValueChanged (Slider* sliderThatWasMoved);

    // Binary resources:
    static const char* fuzzbg_png;
    static const int fuzzbg_pngSize;

    //==============================================================================
    juce_UseDebuggingNewOperator

private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    //[/UserVariables]

    //==============================================================================
    Slider* slider;
    Image* cachedImage_fuzzbg_png;

    //==============================================================================
    // (prevent copy constructor and operator= being generated..)
    NewJucerComponent (const NewJucerComponent&);
    const NewJucerComponent& operator= (const NewJucerComponent&);
};


#endif   // __JUCER_HEADER_NEWJUCERCOMPONENT_FUZZJUCERLAYOUT_6BBFB5F5__
