#include <set>

#include "cinder/app/AppBasic.h"
#include "cinder/gl/gl.h"
#include "cinder/Camera.h"
#include "cinder/params/Params.h"
#include "cinder/Cinder.h"
#include "cinder/Rand.h"
#include "cinder/Timeline.h"

#include "Neuron.h"

using namespace ci;
using namespace ci::app;
using namespace std;
using namespace Neural;

class genetics_cinderApp : public AppBasic
{
	/// Measured in number of cells along each axis.
	static const size_t GRID_LENGTH;
	/// Measured in 3D world units.
	static const float CELL_SIZE;

	/// Type for set of keycodes
	typedef std::set<int> KeySet;

  public:
	/// Called when Cinder wants us to set custom settings for this application.
	void prepareSettings(Settings *settings);
	void setup();
	void shutdown();
	void update();
	void draw();
	void resize(ResizeEvent event);
	void mouseDrag(MouseEvent event);
	
	/// We use WASD to move around the scene.
	void keyDown(KeyEvent event);

	void keyUp(KeyEvent event);

private:
	void _setUpCamera();

	void _handleResize();

	///
	/// Moves the camera around the scene according to key input. 
	///
	/// @param input A set of all the keys that are currently down.
	///
	void _handleKeyInput(KeySet input);

	/// @return True if the set contains the given element, otherwise false.
	template<typename T> bool _setContains(std::set<T> container, T element)
	{
		return container.find(element) != container.end();
	}
	
	/// Converts the voltage potential inside a particular neuron into a color
	/// we can display.
	Colorf _voltageToColor(float voltage);

	//////////// Camera stuff ////////////
	CameraPersp mCam;
	/// Camera position
	Vec3f mEye;
	/// lookAt vector
	Vec3f mCenter;
	/// Camera up vector (y axis in this case)
	Vec3f mUp;
	//////////// End camera stuff ////////////

	/// Updated on mouseDrag event, used to figure out how much
	/// the mouse have moved so we can rotate the camera
	Vec2i mLastMousePos;

	/// Rotation of the entire scene, calculated at mouseDrag and
	/// applied at draw.
	Vec3f mSceneRotation;

	params::InterfaceGl mParams;

	/// All the keys that are currently down (stored as cinder's KeyEvent
	/// keycode).
	KeySet mPressedKeys;

	NeuralNet* mNeuralNet;

	NeuralNet::InputList mNetInputs;

};

const size_t genetics_cinderApp::GRID_LENGTH = 16;
const float genetics_cinderApp::CELL_SIZE = 32.f;

void genetics_cinderApp::prepareSettings(Settings *settings)
{
	settings->setTitle("Genetics II");
	settings->setWindowSize(800, 600);
}

void genetics_cinderApp::setup()
{
	_setUpCamera();

	mSceneRotation = Vec3f::zero();

	mParams = params::InterfaceGl("Statistics", Vec2i(225, 150));

	mNeuralNet = new NeuralNet(4, 4, GRID_LENGTH - 2, GRID_LENGTH * GRID_LENGTH);

	
}

void genetics_cinderApp::shutdown()
{
	delete mNeuralNet;
	mNeuralNet = NULL;
}

void genetics_cinderApp::update()
{
	_handleKeyInput(mPressedKeys);
}

void genetics_cinderApp::resize(ResizeEvent event)
{
	_handleResize();
}

void genetics_cinderApp::mouseDrag(MouseEvent event)
{
	Vec2i mousePos = event.getPos();

	Vec2i rotation = mousePos * 0.6f;
	mSceneRotation = Vec3f(rotation.y, -rotation.x, 0.f);

	mLastMousePos = mousePos;
}

void genetics_cinderApp::keyDown(KeyEvent event)
{
	if(event.isAltDown() && event.getCode() == KeyEvent::KEY_F4)
		quit();

	mPressedKeys.insert(event.getCode());
}

void genetics_cinderApp::keyUp(KeyEvent event)
{
	mPressedKeys.erase(event.getCode());
}

Colorf genetics_cinderApp::_voltageToColor(float voltage)
{
	// return a shade of orange
	return hsvToRGB(Vec3f(0.05, 1.f, voltage * 0.01f));
}

void genetics_cinderApp::draw()
{
	gl::clear();
	
	gl::enableAdditiveBlending();

	mCam.lookAt(mEye, mCenter, mUp);
	gl::setMatrices(mCam);
	
	gl::rotate(Vec3f(mSceneRotation.x, mSceneRotation.y, 0.f));
	
	// Size of entire cube in world units
	const float CUBE_WORLD_SIZE = GRID_LENGTH * CELL_SIZE;

	Vec3f pos;
	float neuronOutput = 0.f;
	for(size_t layerNum = 1; layerNum < mNeuralNet->getNumLayers() - 1; ++layerNum)
	{
		NeuronLayer* layer = mNeuralNet->getLayer(layerNum);

		for(size_t x = 0; x < GRID_LENGTH; ++x)
			for(size_t z = 0; z < GRID_LENGTH; ++z)
			{
				size_t neuronNum = z * GRID_LENGTH + x;
				neuronOutput = layer->at(neuronNum)->getOutput();
			
				Colorf color = _voltageToColor(neuronOutput);

				gl::color(color);

				pos.x = x * CELL_SIZE;
				pos.y = layerNum * CELL_SIZE;
				pos.z = z * CELL_SIZE;

				// This puts the center of the cube at the scene's origin
				pos -= CUBE_WORLD_SIZE / 2.f;

				gl::drawCube(pos, Vec3f(CELL_SIZE, CELL_SIZE, CELL_SIZE));
			}
	}

}

void genetics_cinderApp::_setUpCamera()
{
	_handleResize();

	mEye = Vec3f(500.f, 500.f, 500.f);
	mCenter = Vec3f::zero();
	mUp = Vec3f::yAxis();
}

void genetics_cinderApp::_handleResize()
{
	mCam.setPerspective(60.f, getWindowAspectRatio(), 5.f, 3000.f);
}

void genetics_cinderApp::_handleKeyInput(KeySet input)
{
	static float lastTime = timeline().getStartTime();
	float currentTime = timeline().getCurrentTime();
	float movementAmount = 1000.f * (currentTime - lastTime);
	lastTime = currentTime;

	Quatf camOrientation = mCam.getOrientation();

	if(_setContains<int>(mPressedKeys, KeyEvent::KEY_w))
	{
		mEye += Vec3f(0.f, 0.f, -movementAmount) * camOrientation;
	}
	
	if(_setContains<int>(mPressedKeys, KeyEvent::KEY_s))
	{
		mEye += Vec3f(0.f, 0.f, movementAmount) * camOrientation;
	}
	
	if(_setContains<int>(mPressedKeys, KeyEvent::KEY_a))
	{
		mEye += Vec3f(movementAmount, 0.f, 0.f) * camOrientation;
	}

	if(_setContains<int>(mPressedKeys, KeyEvent::KEY_d))
	{
		mEye += Vec3f(movementAmount, 0.f, 0.f) * camOrientation;
	}

	if(_setContains<int>(mPressedKeys, KeyEvent::KEY_SPACE))
	{
		// TEMP: pushing random inputs to make things interesting
		mNetInputs.clear();

		for(size_t i = 0; i < 4; ++i)
		{
			mNetInputs.push_back((float)rand() / (float)RAND_MAX);
		}
		// end TEMP

		mNeuralNet->update(mNetInputs);
	}

}


CINDER_APP_BASIC( genetics_cinderApp, RendererGl )
