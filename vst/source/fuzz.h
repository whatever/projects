#ifndef _EUPHONIC_FUZZ_H_
#define _EUPHONIC_FUZZ_H_

#include "audioeffectx.h"
#include "Parameter.h"
#include <math.h>

//-------------------------------------------------------------------------------------------------------
class EuphonicFuzz : public AudioEffectX
{
public:
	EuphonicFuzz (audioMasterCallback audioMaster);
	~EuphonicFuzz ();

	// Processing
	virtual void processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames);

	// Program
	virtual void setProgramName (char* name);
	virtual void getProgramName (char* name);

	// Parameters
	virtual void setParameter (VstInt32 index, float value);
	virtual float getParameter (VstInt32 index);
	virtual void getParameterLabel (VstInt32 index, char* label);
	virtual void getParameterDisplay (VstInt32 index, char* text);
	virtual void getParameterName (VstInt32 index, char* text);

	virtual bool getEffectName (char* name);
	virtual bool getVendorString (char* text);
	virtual bool getProductString (char* text);
	virtual VstInt32 getVendorVersion ();

private:
	void _initParameters();

protected:
	ParameterList mParameters;
	float mAttack;
	float mDecay;
	float mPreGain;
	float mPostGain;
	unsigned int mTimeOverTop;
	unsigned int mTimeUnderBottom;


	char programName[kVstMaxProgNameLen + 1];
};

#endif
