# Copyright (c) 2010 Jacob Hart
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

import random
import math
import sys


# randomly generates a population of the given size and returns it.
# each individual has only one chromosome, a string.
def generatePopulation(number, chromosomeLength):
  population = []
  for i in range(number):
    #generate random data for each chromosome
    chromosome = []
    for i in range(chromosomeLength):
      chromosome.append(random.randint(0, 255))
    population.append(chromosome) #add individual to population
  return population

# takes one individual (a single chromosome) and determines its fitness.
# 0 is the best fitness; more negative means worse fitness.
def fitness(chromosome, target):
  fitness = 0

  for i in range(len(chromosome)):
    fitness -= math.fabs(chromosome[i] - ord(target[i]))

  return fitness

def mutate(individual):
  # pick a random position to mutate
  index = random.randint(0, len(individual)-1)

  # now give that position a random value
  individual[index] = random.randint(0, 255)

  return individual

# takes a list of integers and returns them as a string.
def intsToString(ints):
  retVal = ""
  for i in ints:
    retVal += chr(i)
  return retVal

# takes a list of integers and prints them as a string
def printStr(ints):
  print intsToString(ints)

# takes two individuals and breeds them by splitting them at a random
# point and recombinating them. returns two children as a tuple.
def breed(mother, father):
  # where to split the strings?
  splitPoint = random.randint(0, len(mother)-1)

  child1 = mother[:splitPoint] + father[splitPoint:]
  child2 = father[:splitPoint] + mother[splitPoint:]
  return (child1, child2)
      
def main():

  if len(sys.argv) < 5:
    print "usage example: genetic.py --popsize 50 --mut 0.2"
    return

  # get command line arguments
  for i in range(len(sys.argv)):
    arg = sys.argv[i]
    if arg == "--popsize":
      populationSize = int(sys.argv[i+1])
    elif arg == "--mut":
      mutationProbability = float(sys.argv[i+1]) #from 0 to 1

  target = "The quick, brown fox jumped over the lazy dog."

  random.seed()

  # start with a random population
  population = generatePopulation(populationSize, len(target))

  # keep doing successive generations until a completely fit individual is found
  numGenerations = 0
  lastBest = ""
  while True:
    # find the fitness of each individual
    fitnesses = [fitness(individual, target) for individual in population]

    # make a dictionary of fitness mapped to each individual
    fitnessDict = {}
    for i in range(len(fitnesses)):
      fitnessDict[fitnesses[i]] = population[i]
    # order keys so we can find most fit individuals easily
    fitnesses = sorted(fitnesses)
    
    # pick the top one third for breeding
    bestFitnesses = fitnesses[-int(populationSize / 3):]

	# if the most fit individual has reached 0 (optimum fitness), we're done
    if bestFitnesses[-1] == 0:
      print "Found match in", numGenerations, "generations."
      printStr(fitnessDict[bestFitnesses[-1]])
      return

    # using fitness as the key, look up the most fit individuals from the dictionary
    breeders = []
    for key in bestFitnesses:
      breeders.append(fitnessDict[key])

    # remove non-fit individuals from the population, remembering how many we removed
    population = breeders
    gap = populationSize - len(population)

    # randomly breed who's left to replenish population
    for i in range(gap/2):
      # pick two random individuals
      index1 = random.randint(0, len(population) - 1)
      index2 = random.randint(0, len(population) - 1)

      # breed them to get two children
      (child1, child2) = breed(population[index1], population[index2])

      if random.random() < mutationProbability:
        child1 = mutate(child1)
      if random.random() < mutationProbability:
        child2 = mutate(child2)

      population.append(child1)
      population.append(child2)

    best = fitnessDict[bestFitnesses[-1]]
    if best != lastBest:
      lastBest = best
      printStr(best)

    numGenerations += 1

if __name__ == "__main__":
  main()
