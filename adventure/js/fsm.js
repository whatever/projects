// @class FSM
FSM = function()
{
	// ---------------- init ------------
	this.mStates = new Array();
	
	this.addState = function(state)
	{
		this.mStates.push(state);
	}
	
	this.fireEvent = function(eventName)
	{
		this.mCurrentState.fireEvent(eventName);
	}
	
	this.update = function(timeSinceLastCall)
	{
		this.mCurrentState.update();
	}
	
}

// @class FSMState
FSMState = function()
{
	//-------------- Init --------------
	this.mEventPairs = new Array();
	
	//-------------- Mutators -------------
	
	// @param func A function that will be called once when we enter this state.
	this.setEnterFunction = function(func)
	{
		this.mEnterFunction = func;
	}
	
	// @param func A function that will be called on every update tick
	//		while we're in this state.
	this.setUpdateFunction = function(func)
	{
		this.mUpdateFunction = func;
	}
	
	// @param func A function that will be called once when we exit this state.
	this.setExitFunction = function(func)
	{
		this.mExitFunction = func;
	}
	
	// Make this event take a particular action or transit to a particular other state
	// when a given event is fired.
	//
	// @param eventName The (string) name of the event that will cause the action/transition.
	// @param effect A first-class function that represents an action. Can simply be a function to transition
	//			us to a new state.
	this.setEventPair = function(eventName, action)
	{
		var json =
		{
			eventName: eventName,
			action: action
		};
		
		this.mEventPairs.push(json);
	}
	
	//------------------- enter, update, exit for this state. -----------------------
	
	this.enter = function()
	{
		if(this.mEnterFunction)
			this.mEnterFunction();
	}
	
	this.update = function(timeSinceLastCall)
	{
		if(this.mUpdateFunction)
			this.mUpdateFunction(timeSinceLastCall);
	}
	
	this.exit = function()
	{
		if(this.mExitFunction)
			this.mExitFunction();
	}
	
	//-------------------------------------------------------------------
	this.fireEvent = function(eventName)
	{
		//see if we have anything bound to this event.
		for(var i = 0; i < this.mEventPairs.length; i++)
		{
			if(this.mEventPairs[i].eventName == eventName)
			{
				//execution the action.
				this.mEventPairs[i].action;
								
				//we found the thing bound to the event. bail out.
				return;
			}
		}
	}
}