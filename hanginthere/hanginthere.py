import sys
import datetime
import time

##############################################
# Fill in the stats with your own. Dates are YYYY/MM/DD.

# birthdate
BIRTH_DATE = datetime.date(1985, 4, 25)

# total life expectancy in years
LIFE_EXPECTANCY = 75

SAVINGS_START_DATE = datetime.date(2015, 6, 15)
TRAVEL_DATE = datetime.date(2016, 2, 15)

MONEY_SAVED = 5900.00

# You shouldn't have to change anything below this line.
##############################################

def getText():
  localtime = time.localtime(time.time())

  # calculated death date projection
  deathDate = datetime.date(BIRTH_DATE.year + LIFE_EXPECTANCY, BIRTH_DATE.month, BIRTH_DATE.day)

  # calculated age
  currentAge = (datetime.date.today() - BIRTH_DATE).days / 365.0

  text = "\n"

  text += "Today is " + str(localtime[1]) + "/" + str(localtime[2]) + "/" + str(localtime[0]) + ". "

  text += "You are {0:.2f} years old.".format(currentAge)

  text += "\n"

  text += "You are projected to die on {0}. That means you're {1:.2f}% of the way through your life.".format(str(deathDate), 100 * currentAge / LIFE_EXPECTANCY)

  text += "\n\n"

  daysUntilTravel = (TRAVEL_DATE - datetime.date.today()).days
  
  text += "The start of your world journey is {0} days ({1} months) away. ".format(daysUntilTravel, daysUntilTravel / 30.42)
  
  text += "\n\n"
  
  timeElapsed = (datetime.date.today() - SAVINGS_START_DATE).days
  totalTime = (TRAVEL_DATE - SAVINGS_START_DATE).days
  
  percentOfSmallGoal = 100.0 * MONEY_SAVED / 10000.0
  percentOfLargeGoal = 100.0 * MONEY_SAVED / 20000.0
  
  percentOfTimeElapsed = float(timeElapsed) / totalTime * 100.0
  
  text += "You have saved {0}% of your <b>$10,000</b> goal in {1}% of the time.".format(percentOfSmallGoal,  int(percentOfTimeElapsed))
  
  text += "\n"
  
  text += "You have saved {0}% of your <b>$20,000</b> goal in {1}% of the time.".format(percentOfLargeGoal,  int(percentOfTimeElapsed))
    
  text += "\n\n"
  
  text += "You have ${0}.".format(MONEY_SAVED)
  
  text += "\n"
  
  text += "Today's savings goal for <b>$10K</b> is ${0:.2f}".format(10000.0 * percentOfTimeElapsed / 100.0)
  
  text += "\n"
  
  text += "Today's savings goal for <b>$20K</b> is ${0:.2f}".format(20000.0 * percentOfTimeElapsed / 100.0)
  
  text +="\n\n"
  
  dollarsPerDay = MONEY_SAVED / timeElapsed
  hypotheticalTotal = dollarsPerDay * totalTime
  hypotheticalDaysOfTravel = hypotheticalTotal / 55
  
  text += "At this rate, you will have ${0} by your goal date, which will be enough for {1} days of travel.".format(hypotheticalTotal, int(hypotheticalDaysOfTravel))
  
  text += "\n"

  return text

def main():
  sys.stdout.write(getText())
  sys.stdout.flush()
  raw_input("Press any key to continue...");

if __name__ == "__main__":
  main()
