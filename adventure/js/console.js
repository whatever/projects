// @class Console
Console = function()
{
	this.init = function()
	{
		this.mVisible = false;
		this.$window = $("#console");
		
		//this is where the caret will be drawn
		this.mCursorX = 0;
		this.mCursorY = 0;
		
		//counts up to mBlinkTime
		this.mCursorTime = 0;
		
		//time that the cursor takes to blink (in ms)
		this.mBlinkTime = 500;
		//true when the cursor is currently on (blinking)
		this.mCursorOn = true;
		
		//TODO: don't let the scroll bar trigger dragging
		
		this.$window.hide();
		
		this.update(100);
	}
	
	//toggles visibility
	this.toggle = function()
	{
		this.setVisible(!this.mVisible);
	}
	
	this.setVisible = function(visible)
	{
		if(visible == this.mVisible)
			return;
			
		this.mVisible = visible;
			
		if(visible)
		{
			this.$window.show();
			
			$(document).keypress(function(event)
			{
				//remove cursor if it's being displayed
				if(playConsole.mCursorOn)
				{
					playConsole.backspace();
					playConsole.mCursorOn = false;
				}
				
				//handle enter key
				if(event.which == 13)
				{
					playConsole.$window.append("<br>");
					playConsole.carriageReturn();
					return;
				}
				
				playConsole.writechar(String.fromCharCode(event.which));
				
				$("#console").scrollTop(10000);
			});
			
			$(document).keydown(function(event)
			{
				//remove cursor if it's being displayed
				if(playConsole.mCursorOn)
				{
					playConsole.backspace();
					playConsole.mCursorOn = false;
				}
				
				//handle backspace key
				if(event.which == 8)
					playConsole.backspace();
			});
		}
		else
		{
			this.$window.hide();
			
			//remove the key callback since we can't type anymore
			$(document).keydown = 0;
		}
	}
	
	this.update = function(timeSinceLastCall)
	{
		this.updateCursor(timeSinceLastCall);
	}
	
	// This makes the text cursor blink and moves it to the correct position.
	this.updateCursor = function(timeSinceLastCall)
	{
		this.mCursorTime += timeSinceLastCall;
		
		if(this.mCursorTime > this.mBlinkTime)
		{
			this.mCursorTime = 0;
			this.mCursorOn = !(this.mCursorOn);
			
			if(this.mCursorOn)
			{
				this.writechar("&#x25A0;", this.mCursorX);
			}
			else
				this.backspace();
		}
	}
	
	this.writeline = function(text)
	{
		this.$window.append(text + "\n");
		
		//"carriage return" for the cursor
		this.mCursorX = 0;
		this.mCursorY++;
	}
	
	this.writechar = function(ch)
	{
		this.$window.append(ch);
		
		//move the cursor one to the right.
		this.mCursorX++;
	}
	
	this.backspace = function()
	{
		var oldText = this.$window.html();
		
		this.$window.html(oldText.substring(0, oldText.length - 1));
	}
	
	this.carriageReturn = function()
	{
		var lines = this.$window.text().split("\n");
		var lastLine = lines[lines.length-1];
		
		this.evaluate(lastLine);
		
		this.mCursorY++;
		this.mCursorX = 0;
	}
	
	//executes a line in the console.
	this.evaluate = function(line)
	{
		console.log("EXECUTING: " + line);
	}
	
	this.init();
}