using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace NppPluginNET
{
    partial class PluginBase
    {
        #region " Fields "
        internal const string PluginName = "Hex Color Viewer";
        static string iniFilePath = null;
        static string sectionName = "Insert Extension";
        static string keyName = "doCloseTag";
        static bool doCloseTag = false;
        static string sessionFilePath = @"C:\text.session";
        static frmGoToLine frmGoToLine = null;
        static internal int idFrmGotToLine = -1;
        static Bitmap tbBmp = Properties.Resources.star;
        static Bitmap tbBmp_tbTab = Properties.Resources.star_bmp;
        static Icon tbIcon = null;
        #endregion

        #region " Startup/CleanUp "
        static internal void CommandMenuInit()
        {
            // Initialization of your plugin commands
            // You should fill your plugins commands here
 
        	//
	        // Firstly we get the parameters from your plugin config file (if any)
	        //

	        // get path of plugin configuration
            StringBuilder sbIniFilePath = new StringBuilder(Win32.MAX_PATH);
            Win32.SendMessage(nppData._nppHandle, NppMsg.NPPM_GETPLUGINSCONFIGDIR, Win32.MAX_PATH, sbIniFilePath);
            iniFilePath = sbIniFilePath.ToString();

	        // if config path doesn't exist, we create it
            if (!Directory.Exists(iniFilePath))
	        {
                Directory.CreateDirectory(iniFilePath);
	        }

	        // make your plugin config file full file path name
            iniFilePath = Path.Combine(iniFilePath, PluginName + ".ini");

	        // get the parameter value from plugin config
	        doCloseTag = (Win32.GetPrivateProfileInt(sectionName, keyName, 0, iniFilePath) != 0);

            SetCommand(0, "Pick Color/Show Selected As Color", showSelectedAsColor, new ShortcutKey(true, true, false, Keys.H));
        }

        static internal void PluginCleanUp()
        {
	        Win32.WritePrivateProfileString(sectionName, keyName, doCloseTag ? "1" : "0", iniFilePath);
        }
        #endregion

        #region " Menu functions "

        static void showSelectedAsColor()
        {
            //find out how long the selection buffer is (in bytes)
            int selectionSize = (int)Win32.SendMessage(GetCurrentScintilla(), SciMsg.SCI_GETSELTEXT, 0, 0);

            //copy the selected text into our local StringBuilder
            StringBuilder selectedTextBuffer = new StringBuilder(Win32.MAX_PATH);
            Win32.SendMessage(GetCurrentScintilla(), SciMsg.SCI_GETSELTEXT, 0, (selectedTextBuffer));

            string selectedText = selectedTextBuffer.ToString();

            ColorDialog colorDlg = new ColorDialog();
            colorDlg.FullOpen = true;

            bool inputHadHashmark = false;
			bool inputHad0x = false;

            //if the user has selected a hex color, put it in the picker
            if (selectedText.Length > 0)
            {
				//if the input string starts with a hash mark, remove it and remember that it was there
				if (selectedText[0] == '#')
				{
					selectedText = selectedText.Substring(1, selectedText.Length - 1);
					inputHadHashmark = true;
				}
				else if (selectedText.StartsWith("0x"))
				{
					selectedText = selectedText.Substring(2, selectedText.Length - 2);
					inputHad0x = true;
				}

                //if the input is a valid hex string, convert it to a color and put it in the picker.
                if (_isHexString(selectedText))
                {
                    Color userColor = _hexStringToColor(selectedText);
                    colorDlg.Color = userColor;
                }
            }

            DialogResult dialogResult = colorDlg.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                //if we got "OK" from the color dialog, insert hex for the selected color into Notepad++.
                string chosenColor = _colorToHexString(colorDlg.Color);
				//if the user originally selected a color with hashmark intact, add it back in.
				if (inputHadHashmark)
					chosenColor = "#" + chosenColor;
				else if (inputHad0x)
					chosenColor = "0x" + chosenColor;

                StringBuilder sbChosenColor = new StringBuilder(chosenColor);

                Win32.SendMessage(GetCurrentScintilla(), SciMsg.SCI_REPLACESEL, 0, sbChosenColor);
            }
        }

        #endregion

        #region Helper functions
        private static bool _isHexString(string str)
        {
            return Regex.Match(str, "[a-f0-9A-F]+").Value == str;
        }

        /* Expands a three-digit shorthand hex string (as used in CSS) to a full, six-digit
         * hex string. For example, f01 will become ff0011. The leading # character must already
         * be stripped away. If the string is not shorthand, it is returned as is. */
        private static string _expandShorthandHex(string str)
        {
            StringBuilder outStr = new StringBuilder();
            if (str.Length == 3)
            {
                for (int i = 0; i < str.Length; ++i)
                {
                    outStr.Append(str[i]);
                    outStr.Append(str[i]);
                }
            }

            return outStr.ToString();
        }

        /* The leading # character must already be stripped away. */
        private static Color _hexStringToColor(string hexStr)
        {
            //hexStr = _expandShorthandHex(hexStr);

            //convert the hex string into an actual hex value
            Int32 hexVal = Convert.ToInt32(hexStr, 16);

            Int32 red = (hexVal >> 16) & 0xff;
            Int32 green = (hexVal >> 8) & 0xff;
            Int32 blue = hexVal & 0xff;

            return Color.FromArgb(red, green, blue);
        }

        private static string _colorToHexString(Color color)
        {
            //build the red, green, and blue components of the hex code
            Int32 redMask = (color.R << 16);
            Int32 greenMask = (color.G << 8);
            Int32 blueMask = color.B;

            Int32 hexVal = redMask | greenMask | blueMask;

            string retVal = Convert.ToString(hexVal, 16);
            //make sure the hex string is 6 digits long
            retVal = retVal.PadLeft(6, '0');

            return retVal;
        }
        #endregion

    }
}   
