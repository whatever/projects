#include "TimerSystem.h"
#include <string>


namespace CubeControl
{
	TimerSystem::TimerSystem()
		: Singleton()
	{
		QueryPerformanceFrequency(&mTicksPerSecond);
	}

	void TimerSystem::registerCallback(const String& name, TimerCallback callback)
	{
		mCallbacks.insert(std::make_pair(name, callback));
	}

	void TimerSystem::unregisterCallback(const String& name)
	{
		TimerCallbackMap::iterator iter = mCallbacks.find(name);

		if(iter == mCallbacks.end())
			throw new String("Timer callback not found: " + name);

		mCallbacks.erase(iter);
	}

	void TimerSystem::update()
	{
		LARGE_INTEGER ticks;
		QueryPerformanceCounter(&ticks);
		double timeSinceLastCall = ((double)ticks.QuadPart - mLastTicks.QuadPart) / mTicksPerSecond.QuadPart;
		mLastTicks = ticks;

		for(TimerCallbackMap::iterator iter = mCallbacks.begin(); iter != mCallbacks.end(); ++iter)
			(iter->second)(timeSinceLastCall);
	}

}; // end namespace