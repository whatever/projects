#ifndef _CUBECONTROL_H_
#define _CUBECONTROL_H_
#pragma once

#undef UNICODE
#include <string>
#include <windows.h>
#include <math.h>
#include <algorithm>

namespace CubeControl
{
	/// Length across one side, measured in number of LEDs
	static const BYTE CUBE_SIZE = 4;

	enum Face
	{
		FACE_TOP,
		FACE_BOTTOM,
		FACE_LEFT,
		FACE_RIGHT,
		FACE_FRONT,
		FACE_BACK,
		
		/// All LEDs that aren't part of a face
		FACE_INSIDE,

		NUM_FACES
	};

	struct Point2
	{
		BYTE x;
		BYTE y;
	};

	/// For HSL
	struct float3
	{
		float3() : x(0.f), y(0.f), z(0.f) {}
		float3(float x_, float y_, float z_) : x(x_), y(y_) , z(z_) {}

		float3 operator* (const float rhs)
		{
			return float3(x * rhs, y * rhs, z * rhs);
		}

		void operator+= (const float3& rhs)
		{
			x += rhs.x;
			y += rhs.y;
			z += rhs.z;
		}

		float x, y, z;
	};

	/// 3 bytes. Used for color and position.
	struct Point3
	{
		static const Point3 ZERO;

		Point3() : x(0), y(0) , z(0) {}

		Point3(BYTE x_, BYTE y_, BYTE z_) : x(x_), y(y_), z(z_) {}

		/// Returns a Point3 with random x, y, and z.
		/// @param Maximum random value allowed (inclusive).
		static Point3 CreateRandom(size_t max = 255)
		{
			return Point3(rand() % (max + 1), rand() % (max + 1), rand() % (max + 1));
		}

		void operator+= (const Point3& other)
		{
			x += other.x;
			y += other.y;
			z += other.z;
		}

		// Addition with another Point3
		Point3 operator+ (const Point3& rhs) const
		{
			return Point3(x + rhs.x, y + rhs.y, z + rhs.z);
		}

		// Addition with a scalar quantity
		Point3 operator+ (const BYTE rhs) const
		{
			return Point3(x + rhs, y + rhs, z + rhs);
		}

		Point3 operator* (const float rhs) const
		{
			return Point3((BYTE)(x * rhs), BYTE(y * rhs), BYTE(z * rhs));
		}

		Point3 operator/ (const float rhs) const
		{
			return Point3((BYTE)(x / rhs), BYTE(y / rhs), BYTE(z / rhs));
		}

		void operator*= (int mult)
		{
			x *= mult;
			y *= mult;
			z *= mult;
		}

		/// Square of the distance to another 3D point.
		/// (Returned as the square to avoid an expensive sqrt operation).
		float squaredDistanceTo(const Point3& other) const
		{
			float dx = x - other.x;
			float dy = y - other.y;
			float dz = z - other.z;

			return (float)(dx * dx + dy * dy + dz * dz);
		}

		/// If this point has gone beyond the edges of the cube,
		/// wraps it back to zero.
		/// @return True if any coordinate was wrapped around.
		bool wrap()
		{
			bool wrapped;
			if(x >= CUBE_SIZE)
			{
				x = 0;
				wrapped = true;
			}
			if(y >= CUBE_SIZE)
			{
				y = 0;
				wrapped = true;
			}
			if(z >= CUBE_SIZE)
			{
				z = 0;
				wrapped = true;
			}

			return wrapped;
		}

		BYTE x;
		BYTE y;
		BYTE z;
	};

	typedef Point3 Color3;
	typedef std::string String;

	/// Converts an HSL color to RGB. From here:
	/// http://www.geekymonkey.com/Programming/CSharp/RGB2HSL_HSL2RGB.htm
	/// h, s, l are all in the range 0-1
	Color3 HSL2RGB(double h, double sl, double l);

	/// Converts an RGB color to HSL. From here:
	/// http://www.geekymonkey.com/Programming/CSharp/RGB2HSL_HSL2RGB.htm
	float3 RGB2HSL(double r, double g, double b);

	class CubeController
	{
	public:
		// @param portName For example, "COM3"
		CubeController(const String& portName);
		~CubeController();

		Point2 to2DCoords(const Point3& coords);

		void clearToColor(unsigned char r, unsigned char g, unsigned char b);

		void clearToColor(const Color3& color);

		void setPixel(const Point3& pos, const Color3& color);

		/// Swaps the front and back buffers. You have to do this after your
		/// draw calls if you want them to show up.
		void swapBuffers();

		/// Draws a cube shape within the LED grid.
		void drawCube(const Point3& startPos, BYTE size, const Point3& color);
		
		/// Draws a cuboid (elongated cube) within the LED grid.
		void drawCuboid(const Point3& startPos, const Point3& endPos, const Point3& color);

		/// Sets an entire Y column to a single color.
		void setColumn(BYTE x, BYTE z, BYTE height, const Point3& color);

		/// Sets an entire cube face to a single color.
		void setFace(Face face, const Point3& color);

		/// Uses Bresenham's algorithm to draw a 3D line.
		void drawLine(const Point3& start, const Point3& end, const Color3& color);

		void scroll();

	private:
		void _initSerialPort(const String& portName);
		void _closeSerialPort();
		
		HANDLE mSerialPort;
		DWORD mBytesWritten;

	}; // end class CubeController
	
};

#endif