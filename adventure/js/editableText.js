$.fn.extend({ 
	editableText : function() { 
		
		this.click(function() {
		
			//don't do anything if we're already editing
			if(this.editing)
				return true;
			
			var html = $(this).html();
			$(this).html("");
			
			var textArea = document.createElement("textarea");
			$(textArea).html(html);
			textArea.setAttribute("class", "EditableTextArea");
			
			this.appendChild(textArea);
			textArea.focus();
			
			this.editing = true;
			
			return true;
		});
		
		$(this).bind("clickoutside", function(event) {
			this.editing = false;
			
			var textArea = $(".EditableTextArea", this);
			
			//replace the text area with static text
			var html = textArea.val();
			textArea.parent().html(html);
			textArea.remove();			
			
					
		});
		
				
	}
});