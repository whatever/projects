#ifndef _NEURON_H_
#define _NEURON_H_

#include <vector>

namespace Neural
{
	class Neuron;
	
	typedef std::vector<Neuron*> NeuronLayer;
	typedef std::vector<NeuronLayer*> LayerList;
	typedef std::vector<float> OutputList;
	
	class Neuron
	{
	public:
		Neuron(size_t numInputs);

		/// Randomizes all the input weights.
		void generateRandomWeights(size_t numWeights);

		float getOutput() const;

		/// This should only be used by the net this neuron belongs to,
		/// and only in the following circumstances:
		/// This is an input neuron, and we are setting its value
		///		for the first time.
		void _setOutput(float output);

		void calculateOutput(NeuronLayer* previousLayer);

	private:
		/// Returns a random number between 0.f and 1.f, inclusive.
		float _randomClamped();

		/// The weight applied to each input, in order
		std::vector<float> mInputWeights;

		/// The output value (for now, 0 or 1) of this neuron.
		float mOutput;
	};

	class NeuralNet
	{
	public:
		typedef std::vector<float> InputList;

		NeuralNet(size_t numInputs, size_t numOutputs,
			size_t numHiddenLayers, size_t neuronsPerHiddenLayer);

		/// Updates all neurons in all layers (performs one step in the simulation).
		/// @param inputs The stuff to be passed into the input neurons, in order.
		void update(const InputList& inputs);

		NeuronLayer* getLayer(size_t layerNum);
		
		size_t getNumLayers();

		/// @return A vector of the contents of all output neurons.
		OutputList getOutputs();

	private:
		/// Assigns values in an input array to the input neurons.
		void _applyInputs(const InputList& inputs);

		/// Element 0 is the input layer. The last element is the output layer.
		/// Both may contain a different number of elements from the hidden
		/// layers.
		LayerList mLayers;
	};
}
#endif