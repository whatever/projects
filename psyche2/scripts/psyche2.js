//a list of all the images that are displayed in the kaleidoscope
var art = [
"../art/1.jpg",
"../art/2.jpg",
"../art/3.jpg",
"../art/4.jpg",
"../art/5.jpg",
"../art/6.jpg",
"../art/7.jpg",
"../art/8.gif",
"../art/9.jpg",
"../art/10.jpg",
"../art/11.jpg",
"../art/12.jpg"
]

//array index of the current art in the kaleidoscope
var currentArt = 0;

//time we spend on each slide
var timePerArt = 20000;

//id of the interval callback for changing slides
var intervalCallbackID = 0;

//the kaleidoscope.
var kscope;

$(document).ready(function() {

	var canvas = $("#canvas");

	//Called when fullscreen button is clicked
	$(".FullscreenButton").click(function() {

		//move the canvas above everything else.
		canvas.css("z-index", 11);
		
		//add a callback to bring us back from fullscreen.
		canvas.click(function()
		{
			canvas.css("z-index", -1);
		});
	});
	
	var wWidth = $(window).width();
	var wHeight = $(window).height();
	canvas.width(wWidth);
	canvas.height(wHeight);
	canvas.css("left", 0);
	canvas.css("top", 0);
	
	$("#timePerArtSlider").slider({min:0, max:30000, value:10000});
	$("#updateRateSlider").slider({min:1, max:30, value:8});
	
	//Called when "time per art" slider is changed
	$("#timePerArtSlider").bind("slidechange", function(event, ui)
	{
		timePerArt = ui.value;
		
		//update the interval callback with the new value. zero means never change slides.
		clearInterval(intervalCallbackID);
		if(timePerArt != 0)
			intervalCallbackID = setInterval("nextImage()", timePerArt);
			
		//set the accompanying text.
		if(ui.value == 0)
			$("#timePerArtDisplay").html("Time per pattern: Forever");
		else
			$("#timePerArtDisplay").html("Time per pattern: " + parseInt(ui.value / 1000) + " sec");
	});
	
	//change FPS
	$("#updateRateSlider").bind("slidechange", function(event, ui)
	{
		frameRate = ui.value;
		
		//restarting the kaleidoscope will make it have the new framerate
		kscope.stop();
		kscope.start();
	});
	
	
	//Called when window is resized
	$(window).resize(function() {
		var wWidth = $(window).width();
		var wHeight = $(window).height();
		canvas.width(wWidth);
		canvas.height(wHeight);
	});

	
	//start a new kaleidoscope at the first image.
	kscope = new Kaleidoscope('canvas', art[0], 8);
	kscope.start();
	
	//advance to the next image when appropriate.
	intervalCallbackID = setInterval("nextImage()", timePerArt);
});

//destroys any old kaleidoscope which may exist and creates a new one with the next image in the list.
//if we are at the end of the list, start over.
nextImage = function()
{
	//delete old kaleidoscope.
	kscope.stop();
	delete kscope;
	
	++currentArt;
	
	if(currentArt >= art.length)
		currentArt = 0;
	
	//make a new one.
	kscope = new Kaleidoscope('canvas', art[currentArt], 8);
	kscope.start();
}