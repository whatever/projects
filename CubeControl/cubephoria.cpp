#include "maxcpp5.h"
#include "CubeControl.h"
#include "cubephoria.h"
#include <sstream>

namespace CubeControl
{
	Cubephoria::Cubephoria(t_symbol * sym, long ac, t_atom * av)
		: mActiveColor(0, 255, 0), mMode(MODE_WATERFALL), mBPM(0),
		mLineStartPos(CUBE_SIZE, CUBE_SIZE, CUBE_SIZE), mMovementVector(0, 0, 0)
	{
		mCubeController = new CubeControl::CubeController("COM3");
		
		// inlets / outlets. See the Inlets enum for descriptions.
		setupIO((unsigned int)NUM_INLETS, 0);

		// Set up the clock and schedule its first tick
		gClockObject = new t_object();
		gClockObject->m_clock = clock_new((t_object*)gClockObject, (method)updateClock);
		gClockObject->m_interval = 0.03; // time between ticks. This will update snappily for up to 200 BPM
		clock_fdelay(gClockObject->m_clock, gClockObject->m_interval);

		gTimerCallback = std::tr1::bind(&Cubephoria::updateTimer, this, std::tr1::placeholders::_1);

		// initialize stuff for MODE_SINK_FLOAT
		mIsSinking.resize(CUBE_SIZE * CUBE_SIZE);
		mBuoyPositions.resize(CUBE_SIZE * CUBE_SIZE);
		
		for(size_t x = 0; x < CUBE_SIZE; ++x)
			for(size_t z = 0; z < CUBE_SIZE; ++z)
			{
				size_t index = z * CUBE_SIZE + x;
				mIsSinking[index] = (x + z) % 2;
				
				Point3& buoyPos = mBuoyPositions[index];
				buoyPos.x = x;
				buoyPos.z = z;
				if(mIsSinking[index])
					buoyPos.y = CUBE_SIZE - 1;
				else
					buoyPos.y = 0;
			}
	}

	Cubephoria::~Cubephoria()
	{
		mCubeController->clearToColor(0, 0, 0);
		mCubeController->swapBuffers();

		object_free(gClockObject->m_clock);
		delete gClockObject;
		
		delete mCubeController;
		mCubeController = NULL;
	}
	
	void Cubephoria::bang(long whichInlet)
	{
		if(whichInlet == (long)BEAT_INLET)
			notifyBeat();
		else if(whichInlet == (long)MODE_INLET)
			nextMode();
	}

	void Cubephoria::notifyBeat()
	{
		CubeMode mode;
		if(mMode == MODE_CHAOS)
			mode = (CubeMode)(rand() % NUM_CUBE_MODES); // pick a temporary random mode
		else
			mode = mMode;

		switch(mode)
		{
		case MODE_FLASH_SHORT:
			// Remember the color we're using so we can fade from it to black
			mActiveColor = CubeControl::Color3(255, 255, 255);
			mCubeController->clearToColor(mActiveColor);
			mCubeController->swapBuffers();
			break;
		case MODE_FLASH_LONG:
			mCubeController->clearToColor(Color3::CreateRandom());
			mCubeController->swapBuffers();
			break;
		case MODE_PLANES:
			_beatModePlanes();
			break;
		case MODE_FACES:
			_beatModeFaces();
			break;
		case MODE_WIREFRAME:
			_beatModeWireframe();
			break;
		case MODE_HALVES:
			_beatModeHalves();
			break;
		case MODE_SMALL_LARGE:
			_beatModeSmallLarge();
			break;
		case MODE_RUBIKS:
			_beatModeRubiks();
			break;
		case MODE_SINK_FLOAT:
			_beatModeSinkFloat();
			break;
		default:
			break;
		}
	}

	void Cubephoria::updateTimer(float timeSinceLastCall)
	{
		// TODO: make timer updates work in chaos mode.
		if(mMode == MODE_CHAOS)
			return;

		switch(mMode)
		{
		case MODE_FLASH_SHORT:
			_timerModeFlashShort(timeSinceLastCall);
			break;
		case MODE_PLANES:
			_timerModePlanes(timeSinceLastCall);
			break;
		case MODE_RAIN:
			_timerModeRain(timeSinceLastCall);
			break;
		case MODE_WATERFALL:
			_timerModeWaterfall(timeSinceLastCall);
			break;
		case MODE_PLASMA:
			_timerModePlasma(timeSinceLastCall);
			break;
		case MODE_SINK_FLOAT:
			_timerModeSinkFloat(timeSinceLastCall);
			break;
		}

	}

#pragma region Beat functions
	void Cubephoria::_beatModePlanes()
	{
		static std::vector<char> layerPermutation;
		// Index of current element of permutation
		static size_t permIndex = 0;

		// If plane has finished sliding in, do the next one.
		if(mLineStartPos.x >= CUBE_SIZE ||
			mLineStartPos.y >= CUBE_SIZE ||
			mLineStartPos.z >= CUBE_SIZE)
		{
			// If cube is full, pick a new color and direction and start over.
			if(permIndex >= layerPermutation.size())
			{
				mCubeController->clearToColor(mActiveColor);
				mCubeController->swapBuffers();
				mCubeController->clearToColor(mActiveColor);
				mCubeController->swapBuffers();

				mActiveColor = Color3::CreateRandom();

				layerPermutation = _getPermutationInRange(0, CUBE_SIZE);
				permIndex = 0;

				// set up the movement vector
				mMovementVector = Point3::ZERO;
				char axis = rand() % 3;
				if(axis == 0)
					mMovementVector.x = 1;
				else if(axis == 1)
					mMovementVector.y = 1;
				else
					mMovementVector.z = 1;

				mLineStartPos = Point3::ZERO;
				if(mMovementVector.x != 0)
					mLineStartPos.y = layerPermutation[permIndex];
				else if(mMovementVector.y != 0)
					mLineStartPos.z = layerPermutation[permIndex];
				else if(mMovementVector.z != 0)
					mLineStartPos.x = layerPermutation[permIndex];
			}
			else
			{
				mLineStartPos = Point3::ZERO;
				if(mMovementVector.x != 0)
					mLineStartPos.y = layerPermutation[permIndex];
				else if(mMovementVector.y != 0)
					mLineStartPos.z = layerPermutation[permIndex];
				else if(mMovementVector.z != 0)
					mLineStartPos.x = layerPermutation[permIndex];
			}

			++permIndex;

		} // end if plane finished
	} // end _beatModePlanes()

	void Cubephoria::_beatModeFaces()
	{
		static std::vector<char> facePermutation = _getPermutationInRange(0, NUM_FACES);
		static size_t index = 0;

		mCubeController->setFace((Face)facePermutation[index], mActiveColor);
		mCubeController->swapBuffers();
		mCubeController->setFace((Face)facePermutation[index], mActiveColor);
		mCubeController->swapBuffers();

		++index;
		if(index >= facePermutation.size())
		{
			mActiveColor = Color3::CreateRandom();
			index = 0;
		}
	} // end _beatModeFaces()

	void Cubephoria::_beatModeWireframe()
	{
		static bool nextIsWireframe = false;

		mCubeController->clearToColor(mActiveColor);

		if(nextIsWireframe)
		{
			// Clear non-edge pixels by drawing three cuboids			
			// First, two horizontal ones in the shape of a plus sign.
			mCubeController->drawCuboid(Point3(0, 1, 1), Point3(CUBE_SIZE - 1, CUBE_SIZE - 2, CUBE_SIZE - 2), Color3::ZERO);
			mCubeController->drawCuboid(Point3(1, 1, 0), Point3(CUBE_SIZE - 2, CUBE_SIZE - 2, CUBE_SIZE - 1), Color3::ZERO);
			// vertical one
			mCubeController->drawCuboid(Point3(1, 0, 1), Point3(CUBE_SIZE - 2, CUBE_SIZE - 1, CUBE_SIZE - 2), Color3::ZERO);

			mActiveColor = Color3::CreateRandom();
		}

		mCubeController->swapBuffers();
		nextIsWireframe = !nextIsWireframe;
	}

	void Cubephoria::_beatModeHalves()
	{
		static bool nextIsTop = false;
		static const Color3 COLOR(255, 255, 255);

		mCubeController->clearToColor(0, 0, 0);

		if(nextIsTop)
		{
			mCubeController->drawCuboid(Point3(0, CUBE_SIZE / 2, 0), Point3(CUBE_SIZE - 1, CUBE_SIZE - 1, CUBE_SIZE - 1), COLOR);
		}
		else
		{
			mCubeController->drawCuboid(Point3(0, 0, 0), Point3(CUBE_SIZE - 1, (CUBE_SIZE / 2) - 1, CUBE_SIZE - 1), COLOR);
		}

		mCubeController->swapBuffers();
		nextIsTop = !nextIsTop;
	}

	void Cubephoria::_beatModeSmallLarge()
	{
		static bool nextIsSmall = false;
		
		mCubeController->clearToColor(0, 0, 0);
		mActiveColor = Color3::CreateRandom();

		if(nextIsSmall)
		{
			mCubeController->drawCube(Point3(1, 1, 1), CUBE_SIZE - 2, mActiveColor);
		}
		else
		{
			mCubeController->clearToColor(mActiveColor);
		}
		
		mCubeController->swapBuffers();

		nextIsSmall = !nextIsSmall;
	}

	void Cubephoria::_beatModeRubiks()
	{
		mActiveColor = Color3::CreateRandom();

		/// True when the cube is full and we need to start over.
		static bool reset = true;

		static std::vector<char> x;
		static std::vector<char> y;
		static std::vector<char> z;

		// Indexes into coordinate arrays
		static size_t xi = 0, yi = 0, zi = 0;

		if(reset)
		{
			reset = false; 
			x = _getPermutationInRange(0, 2);
			y = _getPermutationInRange(0, 2);
			z = _getPermutationInRange(0, 2);

			xi = yi = zi = 0;

			mCubeController->clearToColor(0, 0, 0);
			mCubeController->swapBuffers();
			mCubeController->clearToColor(0, 0, 0);
		}

		// draw to both front and back buffers
		for(size_t i = 0; i < 2; ++i)
		{
			mCubeController->drawCube(Point3(x[xi] * 2, y[yi] * 2, z[zi] * 2), 2, mActiveColor);
			mCubeController->swapBuffers();
		}

		// Prepare for the next frame
		++xi;
		if(xi == x.size())
		{
			xi = 0;
			++yi;

			if(yi == y.size())
			{
				yi = 0;
				++zi;

				if(zi == z.size())
				{
					reset = true;
					return;
				}
			}
		}
	} // end _beatModeRubiks()

	void Cubephoria::_beatModeSinkFloat()
	{
		// Reverse the state of some of the buoys
		for(size_t i = 0; i < mIsSinking.size(); ++i)
			if(rand() % 2)
				mIsSinking[i] = !mIsSinking[i];
	}

	void Cubephoria::_timerModeRain(float timeSinceLastCall)
	{
		static float timeUntilUpdate = 0.f;

		timeUntilUpdate -= timeSinceLastCall;
		if(timeUntilUpdate > 0)
			return;

		timeUntilUpdate = 50.f;

		// Measured in LEDs per update.
		static const float GRAVITY = 1.f;

		// Number of total raindrops that can exist at one time.
		// When an old one moves out of the field of view, a new
		// one is created.
		static const BYTE NUM_RAINDROPS = 8;

		/// Length of the whole raindrop in pixels.
		static const BYTE DROP_LENGTH = 3;

		static const Color3 BASE_COLOR = Color3(0, 0, 255);

		/// Position of the bottom of each raindrop.
		static std::vector<float3> raindrops;

		mCubeController->clearToColor(Color3::ZERO);

		while(raindrops.size() < NUM_RAINDROPS)
			raindrops.push_back(float3(rand() % CUBE_SIZE, CUBE_SIZE - 1, rand() % CUBE_SIZE));

		float3 pos;
		Point3 color;
		for(std::vector<float3>::iterator iter = raindrops.begin(); iter != raindrops.end(); ++iter)
		{
			pos = *iter;
			color = BASE_COLOR;
			// draw one drop with trail
			for(size_t i = 0; i < DROP_LENGTH; ++i)
			{
				if(pos.y >= 0)
					mCubeController->setPixel(Point3(pos.x, pos.y, pos.z), color);
				++(pos.y);
				// increase green channel to make trails more teal
				color.y += 40;
				color.z -= 50;
			}

			((*iter).y) -= GRAVITY;

			// erase raindrops when their trails have left the cube
			if((signed short int)((*iter).y) + DROP_LENGTH < 0)
				iter = raindrops.erase(iter);
		}
		
		mCubeController->swapBuffers();
	}

	void Cubephoria::_timerModeWaterfall(float timeSinceLastCall)
	{
		static unsigned short int phase = 0;
		phase += 1;
		if(phase > 2)
			phase = 0;

		static CubeControl::Point3 col1;
		static CubeControl::Point3 col2;

		if(phase == 0)
		{
			col1 = CubeControl::Point3(255, 0, 0);
			col2 = CubeControl::Point3(0, 255, 0);
		}
		else if(phase == 1)
		{
			col1 = CubeControl::Point3(0, 255, 0);
			col2 = CubeControl::Point3(0, 0, 255);
		}
		else if(phase == 2)
		{
			col1 = CubeControl::Point3(0, 0, 255);
			col2 = CubeControl::Point3(255, 0, 0);
		}

		CubeControl::Point3 pos;
		for(pos.x = 0; pos.x < CUBE_SIZE; ++pos.x)
			for(pos.y = 0; pos.y < CUBE_SIZE; ++pos.y)
				for(pos.z = 0; pos.z < CUBE_SIZE; ++pos.z)
				{
					if((pos.x + pos.y + pos.z) % 2)
						mCubeController->setPixel(pos, col1);
					else
						mCubeController->setPixel(pos, col2);

					mCubeController->swapBuffers();
				}

		mCubeController->swapBuffers();
	}

#pragma endregion

#pragma region Timer update functions
	void Cubephoria::_timerModePlanes(float timeSinceLastCall)
	{
		static double timeUntilUpdate = 0;

		// Find an endpoint for the line such that it stretches across
		// the cube
		Point3 lineEndPos = mLineStartPos;
		if(mMovementVector.x != 0)
			lineEndPos.z = CUBE_SIZE - 1;
		else if(mMovementVector.y != 0)
			lineEndPos.x = CUBE_SIZE - 1;
		else if(mMovementVector.z != 0)
			lineEndPos.y = CUBE_SIZE - 1;

		timeUntilUpdate -= timeSinceLastCall;

		if(timeUntilUpdate <= 0.0 &&
			mLineStartPos.x < CUBE_SIZE &&
			mLineStartPos.y < CUBE_SIZE &&
			mLineStartPos.z < CUBE_SIZE &&
			mBPM > 0)
		{
			double secondsPerBeat = (1.0 / mBPM) * 60.0;
			timeUntilUpdate = (secondsPerBeat / (double)CUBE_SIZE) * 30.0;

			mCubeController->drawLine(mLineStartPos, lineEndPos, mActiveColor);
			mCubeController->swapBuffers();
			mCubeController->drawLine(mLineStartPos, lineEndPos, mActiveColor);
			mCubeController->swapBuffers();

			mLineStartPos += mMovementVector;
		}
	}

	void Cubephoria::_timerModeFlashShort(float timeSinceLastCall)
	{
		// Fade the color out quickly
		float3 hsl = RGB2HSL(mActiveColor.x, mActiveColor.y, mActiveColor.z);
		hsl.z *= (1.0 - timeSinceLastCall);
		if(hsl.z < 0.001)
			return;
		mActiveColor = HSL2RGB(hsl.x, hsl.y, hsl.z);
		mCubeController->clearToColor(mActiveColor);
		mCubeController->swapBuffers();
	}

	void Cubephoria::_timerModePlasma(float timeSinceLastCall)
	{
		static float time = 0;
		time += timeSinceLastCall;

		float col1 = 0, col2 = 0, col3 = 0;
		float3 finalColor;
		for(BYTE x = 0; x < CUBE_SIZE; ++x)
			for(BYTE y = 0; y < CUBE_SIZE; ++y)
				for(BYTE z = 0; z < CUBE_SIZE; ++z)
				{
					col1 = sin((float)x + time);
					finalColor.x = fabs(col1);

					col2 = cos((float)y + time);
					finalColor.y = fabs(col2);

					col3 = (sin((float)z + time) + cos((float)z + time));
					finalColor.z = fabs(col3);


					mCubeController->setPixel(Point3(x, y, z), HSL2RGB(finalColor.x, finalColor.y, finalColor.z));
				}

		mCubeController->swapBuffers();

	} // end timerModePlasma

	void Cubephoria::_timerModeSinkFloat(float timeSinceLastCall)
	{
		/// In number of pixels per update. This accounts for both
		/// buoyancy and gravity (the sign is simply flipped).
		static const BYTE SPEED = 1.f;
		/// All buoys are the same color
		static const Color3 COLOR(64, 130, 109);

		static float timeUntilUpdate = 0.f;

		timeUntilUpdate -= timeSinceLastCall;
		if(timeUntilUpdate > 0)
			return;

		timeUntilUpdate = 20.f;

		mCubeController->clearToColor(Color3::ZERO);

		// Update all buoys
		for(size_t i = 0; i < mBuoyPositions.size(); ++i)
		{
			bool isSinking = mIsSinking[i];

			// Move the buoy, keeping it within the cube
			Point3& buoyPos = mBuoyPositions[i];
			if(isSinking && buoyPos.y >= SPEED)
				buoyPos.y -= SPEED;
			else if(!isSinking && buoyPos.y < CUBE_SIZE - SPEED)
				buoyPos.y += SPEED;

			mCubeController->setPixel(mBuoyPositions[i], COLOR);
		}

		mCubeController->swapBuffers();

	}

#pragma endregion

	void Cubephoria::onLong(long whichInlet, long value)
	{
		if(whichInlet == MODE_INLET)
			setMode((CubeMode)value);
	}

	void Cubephoria::onDouble(long whichInlet, double value)
	{
		if(whichInlet == BPM_INLET)
		{
			setBPM(value);
		}
	}
	
	void Cubephoria::setMode(CubeMode mode)
	{
		if(mode < NUM_CUBE_MODES)
			mMode = (CubeMode)mode;
	}

	void Cubephoria::setBPM(double bpm)
	{
		mBPM = bpm;
	}

	void Cubephoria::nextMode()
	{
		mMode = (CubeMode)((int)mMode + 1);

		if((int)mMode >= (int)NUM_CUBE_MODES)
			mMode = (CubeMode)0;
	}

	std::vector<char> Cubephoria::_getPermutationInRange(unsigned char start, unsigned char end)
	{
		std::vector<char> retVal;

		// Start by creating a set of all numbers in the range
		for(size_t i = start; i < end; ++i)
			retVal.push_back(i);

		// Perform a Knuth shuffle to generate a random permutation
		for(size_t i = 0; i < retVal.size(); ++i)
		{
			size_t otherIndex = rand() % retVal.size();
			char temp = retVal[i];
			retVal[i] = retVal[otherIndex];
			retVal[otherIndex] = temp;
		}

		return retVal;
	}

	void updateClock(t_object *x)
	{
		// notify timer listener
		gTimerCallback(gClockObject->m_interval);
		
		// schedule another tick
		clock_fdelay(gClockObject->m_clock, gClockObject->m_interval);
	}

}; // end CubeControl namespace

extern "C" int main(void) {
	// create a class with the given name:
	CubeControl::Cubephoria::makeMaxClass("cubephoria");
	REGISTER_METHOD(CubeControl::Cubephoria, bang);
	INLET_FLOAT(CubeControl::Cubephoria, onDouble);
	INLET_LONG(CubeControl::Cubephoria, onLong);
}