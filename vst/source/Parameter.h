#include <string>
#include <vector>

/// Defines a single VST parameter, including its name, units, etc.
struct Parameter
{
	std::string name;
	std::string units;
	float defaultValue;
};

typedef std::vector<Parameter> ParameterList;