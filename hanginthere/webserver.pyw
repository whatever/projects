# adapted from http://python.about.com/od/networkingwithpython/ss/PythonWebServer.htm

import sys
import time
import socket
import datetime
import hanginthere

host = '' 
port = 80 

c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
c.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

c.bind((host, port))
c.listen(1)

def log(logfile, str):
  timeStr = "<" + time.asctime( time.localtime(time.time()) ) + ">"
  print timeStr, str
  logfile.write(timeStr + " " + str + "\n");
  logfile.flush()

def main():
  logfile = open('webserver.log', 'w')
  log(logfile, "Server starting...\n")
  
  while True:
    try:
      csock, caddr = c.accept()
      cfile = csock.makefile('rw', 0)
      while True:
        line = cfile.readline().strip()
        if len(line) == 0:
          break
      cfile.write("<title>Hang in there</title>")
      cfile.write("<pre>")
      cfile.write(hanginthere.getText())
      cfile.write("</pre>")
      cfile.close()
      csock.close()
    except socket.error:
	  log(logfile, str(socket.error))
	  continue
  
  logfile.close()

if __name__ == "__main__":
  main()
  
