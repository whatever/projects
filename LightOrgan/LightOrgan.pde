#include <CapSense.h>

// Pins 0 and 1 are reserved for Tx/Rx. Do not use

const unsigned int CAP_SEND_PIN = 2;

const unsigned int CAP_RECV_R_PIN = 3;
const unsigned int CAP_RECV_G_PIN = 4;
const unsigned int CAP_RECV_B_PIN = 5;

// Input for mode change button. If you're unfamiliar with pulldown
// resistors and such, check this:
// http://arduino.cc/en/Tutorial/ButtonStateChange
// I chose A0 for circuit layout reasons, even though a
// digital pin would be more appropriate.
const unsigned int MODE_BUTTON_PIN = A0;

// LED out pins (project uses common-anode LEDs, so pull these pins LOW
// to light them, HIGH to turn them off). Pins 9-11 were chosen due
// to their PWM capabilities.
const unsigned int LED_R_PIN = 9;
const unsigned int LED_G_PIN = 10;
const unsigned int LED_B_PIN = 11;

/// Capacitive sensors. 10 MOhm resistor goes on send pin
CapSense   csR = CapSense(CAP_SEND_PIN, CAP_RECV_R_PIN);
CapSense   csG = CapSense(CAP_SEND_PIN, CAP_RECV_G_PIN);
CapSense   csB = CapSense(CAP_SEND_PIN, CAP_RECV_B_PIN);

/// Lookup table populated with a 1Hz digital sine wave normalized from
/// 0 to 255. Used for fading LEDs. See sineTableSetup();
unsigned char sineTable[32];

bool pushButtonState = false;
bool lastPushButtonState = false;

enum ColorMode
{
	/// Fades between colors using a sine wave
	MODE_SINE = 0,
	MODE_ORGAN,
	/// Stops updating the LEDs so we're left with the last
	/// color input in organ mode
	MODE_HOLD,
	NUM_MODES
};

ColorMode currentMode = MODE_SINE;

void setup()
{
	Serial.begin(9600);

	// Capacitive send pin.
	pinMode(CAP_SEND_PIN, OUTPUT);

	// Capacitive receive pins
	pinMode(CAP_RECV_R_PIN, INPUT);
	pinMode(CAP_RECV_G_PIN, INPUT);
	pinMode(CAP_RECV_B_PIN, INPUT);

	pinMode(MODE_BUTTON_PIN, INPUT);

	// LED out pins
	pinMode(LED_R_PIN, OUTPUT);
	pinMode(LED_G_PIN, OUTPUT);
	pinMode(LED_B_PIN, OUTPUT);

	sineTableSetup();
}

void loop()
{
	readModeButton();
	
	switch(currentMode)
	{
	case MODE_SINE:
		updateSineMode();
		break;
	case MODE_ORGAN:
		updateOrganMode();
		break;
	case MODE_HOLD:
		break;
	}
}

/// Reads the pushbutton and advances the mode if there's a
/// new button-press.
void readModeButton()
{
	pushButtonState = analogRead(A0) > 511;
	// if we have a new button-press
	if(pushButtonState && (pushButtonState != lastPushButtonState))
	{
		switch(currentMode)
		{
		case MODE_SINE:
			// switch to organ and don't go back except on power-cycle.
			currentMode = MODE_ORGAN;
			break;
		case MODE_ORGAN:
			currentMode = MODE_HOLD;
			break;
		case MODE_HOLD:
			currentMode = MODE_ORGAN;
			break;
		}
	}
	lastPushButtonState = pushButtonState;
}

void updateSineMode()
{
	static size_t sineTableIndexR = 0;
	static size_t sineTableIndexG = 10;
	static size_t sineTableIndexB = 20;

	unsigned char r = sineTable[sineTableIndexR++];
	unsigned char g = sineTable[sineTableIndexG++];
	unsigned char b = sineTable[sineTableIndexB++];

	if(sineTableIndexR >= 32)
		sineTableIndexR = 0;
	if(sineTableIndexG >= 32)
		sineTableIndexG = 0;
	if(sineTableIndexB >= 32)
		sineTableIndexB = 0;

	analogWrite(LED_R_PIN, 255 - r);
	analogWrite(LED_G_PIN, 255 - g);
	analogWrite(LED_B_PIN, 255 - b);

	// Slow the color pulse down so it's more soothing.
	delay(90);
}

/// Takes input from the three capacitance sensors and uses
/// it to drive the RGB LEDs.
void updateOrganMode()
{
	long capR =  csR.capSense(30);
	long capG =  csG.capSense(30);
	long capB =  csB.capSense(30);

	/*Serial.print(capR);
	Serial.print("\t");
	Serial.print(capG);
	Serial.print("\t");
	Serial.print(capB);
	Serial.println("\t");
	delay(5);*/

	transformCapToLED(capR);
	transformCapToLED(capG);
	transformCapToLED(capB);

	analogWrite(LED_R_PIN, 255 - capR);
	analogWrite(LED_G_PIN, 255 - capG);
	analogWrite(LED_B_PIN, 255 - capB);
}

/// @param A raw value from a capacitance sensor. Will be
///		updated to a color value from 0 to 255, which can
///		then be fed into an LED.
void transformCapToLED(long& cap)
{
	cap -= 750;
	cap = (float)cap * 255 / 2000;
	cap = constrain(cap, 0, 255);
}

void sineTableSetup()
{
	// Put 32 step 8 bit sine table into array.
	sineTable[0]=127;
	sineTable[1]=152;
	sineTable[2]=176;
	sineTable[3]=198;
	sineTable[4]=217;
	sineTable[5]=233;
	sineTable[6]=245;
	sineTable[7]=252;
	sineTable[8]=254;
	sineTable[9]=252;
	sineTable[10]=245;
	sineTable[11]=233;
	sineTable[12]=217;
	sineTable[13]=198;
	sineTable[14]=176;
	sineTable[15]=152;
	sineTable[16]=128;
	sineTable[17]=103;
	sineTable[18]=79;
	sineTable[19]=57;
	sineTable[20]=38;
	sineTable[21]=22;
	sineTable[22]=10;
	sineTable[23]=3;
	sineTable[24]=0;
	sineTable[25]=3;
	sineTable[26]=10;
	sineTable[27]=22;
	sineTable[28]=38;
	sineTable[29]=57;
	sineTable[30]=79;
	sineTable[31]=103;
}