#include "Neuron.h"
#include <stdlib.h>
#include <time.h>
#include <string>
#include "Util.h"

namespace Neural
{
	Neuron::Neuron(size_t numInputs) : mOutput(0)
	{
		generateRandomWeights(numInputs);
	}

	void Neuron::generateRandomWeights(size_t numWeights)
	{
		mInputWeights.clear();

		// Create one weight for each input
		for(size_t i = 0; i < numWeights; ++i)
		{
			mInputWeights.push_back(_randomClamped() * 0.02f);
		}
	}

	float Neuron::getOutput() const
	{
		return mOutput;
	}

	void Neuron::_setOutput(float output)
	{
		mOutput = output;
	}

	void Neuron::calculateOutput(NeuronLayer* previousLayer)
	{
		mOutput = 0.f;
		
		assert(mInputWeights.size() == previousLayer->size());

		// weight all inputs, adding them to the output
		size_t i = 0;
		for(NeuronLayer::const_iterator iter = previousLayer->begin(); iter != previousLayer->end(); ++iter)
		{
			mOutput += (*iter)->getOutput() * mInputWeights[i];
			++i;
		}
	}

	float Neuron::_randomClamped()
	{
		return (float)rand() / (float)RAND_MAX;
	}

	NeuralNet::NeuralNet(size_t numInputs, size_t numOutputs,
			size_t numHiddenLayers, size_t neuronsPerHiddenLayer)
	{
		if(numHiddenLayers < 1)
			throw std::string("Must have at least one hidden layer.");
		if(neuronsPerHiddenLayer < 1)
			throw std::string("Must have at least one neuron per hidden layer.");
		
		// Create the input layer, which itself has no inputs
		NeuronLayer* inputLayer = new NeuronLayer();
		for(size_t i = 0; i < numInputs; ++i)
			inputLayer->push_back(new Neuron(0));
		mLayers.push_back(inputLayer);


		// Create all the hidden (intermediate) layers and populate them with neurons
		for(size_t l = 1; l < numHiddenLayers; ++l)
		{
			NeuronLayer* layer = new NeuronLayer();
			mLayers.push_back(layer);

			// First hidden layer gets input from every input neuron.
			// Each other hidden layer gets input from the one before it.
			const size_t NUM_INPUTS = (l == 1) ? numInputs : neuronsPerHiddenLayer;

			for(size_t n = 0; n < neuronsPerHiddenLayer; ++n)
			{
				layer->push_back(new Neuron(NUM_INPUTS));
			}
		}

		// Output layer receives inputs from previous hidden layer
		NeuronLayer* outputLayer = new NeuronLayer();
		for(size_t i = 0; i < numOutputs; ++i)
			outputLayer->push_back(new Neuron(neuronsPerHiddenLayer));
		mLayers.push_back(outputLayer);
	}

	void NeuralNet::update(const InputList& inputs)
	{
		// Populate the first layer manually
		_applyInputs(inputs);

		// Each other hidden layer gets its inputs from the previous one.
		// TODO: Could be rewritten with iterators.
		for(size_t i = 1; i < mLayers.size(); ++i)
		{
			NeuronLayer* layer = mLayers[i];
			NeuronLayer* previousLayer = mLayers[i-1];

			for(NeuronLayer::iterator iter = layer->begin(); iter != layer->end(); ++iter)
			{
				Neuron* neuron = *iter;

				neuron->calculateOutput(mLayers[i-1]);
			}
		}

		// The output layer get its values from the last hidden layer.
		
	}

	NeuronLayer* NeuralNet::getLayer(size_t layerNum)
	{
		return mLayers[layerNum];
	}

	size_t NeuralNet::getNumLayers()
	{
		return mLayers.size();
	}

	OutputList NeuralNet::getOutputs()
	{
		OutputList retVal;
		
		NeuronLayer* outputLayer = mLayers.back();
		for(NeuronLayer::const_iterator iter = outputLayer->begin(); iter != outputLayer->end(); ++iter)
			retVal.push_back((*iter)->getOutput());
		
		return retVal;
	}

	void NeuralNet::_applyInputs(const InputList& inputs)
	{
		NeuronLayer* inputLayer = mLayers[0];
		if(inputs.size() != inputLayer->size())
			throw std::string("Number of inputs must match number of input neurons");

		// TODO: It's probably more efficient to use iterators
		for(size_t i = 0; i < inputLayer->size(); ++i)
			(inputLayer->at(i))->_setOutput(inputs[i]);
	}



}