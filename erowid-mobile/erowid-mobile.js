var drugNames;

$(document).ready(function()
{
	showLoading();
	
	drugNames = scrapeDrugNames();
	
	clearDocument();
	
	createMenu(drugNames);
	
	initAutocomplete();
});

scrapeDrugNames = function()
{
	var drugNames = new Array();
	
	$.each($("option"), function(index, value)
	{
		var $value = $(value);
		
		var name = $value.text();
		var link = "vault.php?link=" + encodeURI("http://www.erowid.org" + $value.val());
		
		//skip non-drug items
		if(name[0] != "-" && name != "Main Index" && name != "Big Chart"
			&& name != "Common Psychoactives" && name != "Chemicals" && name != "Plants"
			&& name != "Smart Drugs" && name != "Pharmaceuticals" && name != "Herbs")
		{
			var json = {drugName: name, drugLink: link};
		
			drugNames.push(json);
		}
	})
	
	return drugNames;
}

//shows a loading indicator while we are scraping the drug names.
showLoading = function()
{
	var div = document.createElement("div");
	div.setAttribute("class", "Loading");
	$(div).text("Loading...");
	
	document.body.appendChild(div);
}

clearDocument = function()
{
	$("head").html('<link rel="stylesheet" type="text/css" href="erowid-mobile.css" />');
	$("body").html("");
}

createMenu = function(drugNames)
{
	//create the menu div
	var menu = document.createElement("div");
	menu.setAttribute("id", "menu");
	document.body.appendChild(menu);

	var index;
	$.each(drugNames, function(index, value)
	{
		createMenuItem(value.drugName, value.drugLink, index);
		index++;
	});
}

clearMenu = function()
{
	$("#menu").remove();
}

createMenuItem = function(name, link, index)
{
	var menuItem = document.createElement("a");
	menuItem.setAttribute("id", name);
	menuItem.setAttribute("class", "DrugMenuItem");
	menuItem.setAttribute("href", link);
	menuItem.style.top = index * 20 + "%";
	$(menuItem).text(name);
	
	document.getElementById("menu").appendChild(menuItem);
}

initAutocomplete = function()
{
	var textbox = document.createElement("input");
	textbox.setAttribute("class", "Autocomplete");
	textbox.setAttribute("id", "drugName");
	
	$(textbox).val("Enter drug name");
	$(textbox).keyup(autocompleteKeyPressed);
	$(textbox).click(autocompleteClicked);
	
	document.body.appendChild(textbox);
}

autocompleteKeyPressed = function(event)
{
	var text = $(event.target).val();
	
	var results = new Array();
	//iterate through all the drug names
	$.each(drugNames, function(index, value)
	{
		//if the drug name starts with the autocomplete string,
		//add it to the results
		if(value.drugName.toLowerCase().indexOf(text.toLowerCase()) == 0)
			results.push(value);
	});
	
	clearMenu();
	
	createMenu(results);
}

autocompleteClicked = function(event)
{
	var $box = $(this);
	
	//clear the textbox, but only once
	$box.val("");
	$box.unbind("click");
}