# Copyright (c) 2010 Jacob Hart
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

import genetics
from Tkinter import *
from PIL import Image
from PIL import ImageTk
import random



def main():
  if len(sys.argv) < 9:
    print "usage example: genetics-image.py --popsize 50 --mut 0.2 --image myImage.png --zoom 4"
    return

  # get command line arguments
  for i in range(len(sys.argv)):
    arg = sys.argv[i]
    if arg == "--popsize":
      populationSize = int(sys.argv[i+1])
    elif arg == "--mut":
      mutationProbability = float(sys.argv[i+1]) # from 0 to 1
    elif arg == "--image":
      target = Image.open(sys.argv[i+1])
    elif arg == "--zoom":
      zoom = int(sys.argv[i+1])
     
  # get the target image as a series of int colorvalues
  targetData = target.tostring()

  # width and height of target image.
  width = target.size[0]
  height = target.size[1]
  # pixel depth in bytes
  depth = len(target.mode)


  print "Generating random population..."
  random.seed()
  population = genetics.generatePopulation(populationSize, len(targetData))
  print "Done. Evolving..."

  tk = Tk()

  canvas = Canvas(tk, height = height * zoom, width = width * zoom)
  canvas.pack(side=LEFT, fill=BOTH, expand=1)
  tk.title("Evolving...")

  # create another canvas to show the target image
  targetCanvas = Canvas(tk, height = height * zoom, width = width * zoom)
  targetCanvas.pack(side=RIGHT, fill=BOTH, expand=1)

  target = target.resize((width * zoom, height * zoom), Image.ANTIALIAS)
  targetPhoto = ImageTk.PhotoImage(target)
  item = targetCanvas.create_image(0, 0, anchor=NW, image=targetPhoto)

  numGenerations = 0
  lastBest = ""
  while True:
    # find the fitness of each individual
    fitnesses = [genetics.fitness(individual, targetData) for individual in population]

    # make a dictionary of fitness mapped to each individual
    fitnessDict = {}
    for i in range(len(fitnesses)):
      fitnessDict[fitnesses[i]] = population[i]
    # order keys so we can find most fit individuals easily
    fitnesses = sorted(fitnesses)

    # pick the top 25% for breeding
    bestFitnesses = fitnesses[-int(populationSize / 3):]

    if bestFitnesses[-1] == 0:
      print "Found match in", numGenerations, "generations."

    # using fitness as the key, look up the most fit individuals from the dictionary
    breeders = []
    for key in bestFitnesses:
      breeders.append(fitnessDict[key])

    # remove non-fit individuals from the population, remembering how many we removed
    population = breeders
    gap = populationSize - len(population)

    # randomly breed who's left to replenish population
    for i in range(gap/2):
      # pick two random individuals
      index1 = random.randint(0, len(population) - 1)
      index2 = random.randint(0, len(population) - 1)

      # breed them to get two children
      (child1, child2) = genetics.breed(population[index1], population[index2])

      if random.random() < mutationProbability:
        child1 = genetics.mutate(child1)
      if random.random() < mutationProbability:
        child2 = genetics.mutate(child2)

      population.append(child1)
      population.append(child2)

    best = fitnessDict[bestFitnesses[-1]]
    if best != lastBest:
      lastBest = best
      canvas.delete("*")

      bestStr = genetics.intsToString(best)

      image = Image.fromstring(target.mode, (width, height), bestStr)
      image = image.resize((width * zoom, height * zoom), Image.ANTIALIAS)
      photo = ImageTk.PhotoImage(image)
      item = canvas.create_image(0, 0, anchor=NW, image=photo)

      canvas.create_text((10, 10), text="Generation: " + str(numGenerations), anchor=NW, fill="white", font=("Arial Bold", 18))

      tk.title("Generation: " + str(numGenerations) + ". Evolving...")
      

    numGenerations += 1


    tk.update()

if __name__ == "__main__":
  main()
