#pragma once
#include <windows.h>
#include "../../CubeControl/CubeControl.h"

enum CubeEventType
{
	NO_OP = 0,
	// Argument will be mode number
	CHANGE_MODE,
	// Sets the whole cube to a static color. Mode will be
	// ignored until it's set again.
	SET_COLOR,
	// Goes to the next mode. No argument
	NEXT_MODE,
	// Argument will be the number of seconds between random
	// mode changes. 0 means randomness will be turned off.
	RANDOM,
	// Argument is update speed as a percentage
	SPEED,
	// Argument is 1 to turn the cube on and 0 to turn it off
	POWER,
	// Argument will be the name of the port to change to (1 for COM1, etc.)
	CHANGE_PORT,
	
	NUM_EVENT_TYPES
};

enum CubeMode
{
	// ascending "X" shape
	MODE_X,
	MODE_RUBIKS_CUBE,
	MODE_BOUNCING_CUBE,
	MODE_FLASH_COLORS,
	MODE_STROBE_LIGHT,
	MODE_FADE_COLORS,
	// cycle through random colors for edges, interior
	MODE_EDGES,
	MODE_COMET,
	// quickly flashing RGB that looks white
	RGB_MAGIC,
	MODE_WATERFALL,
	// fade through the colors of the rainbow
	MODE_RAINBOW,

	NUM_MODES
};

struct CubeEvent
{
	CubeEvent() : type(NO_OP), argument(0) {}

	CubeEventType type;
	int argument;
	// Used only for the SET_COLOR event type. Describes
	// a static RGB color to set to.
	CubeControl::Color3 color;
};

int main();

// Processes one line from the console. After this function
// is done, no further action needs to be taken. There is
// one exception: If this function returns false, the
// application should end.
bool processConsoleCommand(char* command);

// Prints a list of possible commands to the console.
void printHelp();

// Takes a screenshot of the entire screen, then returns the
// overall average color.
CubeControl::Color3 getAverageScreenColor();

// Returns true on success
bool createCubeUpdateThread();

// Uses the serial port to update the cube. Spawned
// as its own thread.
DWORD WINAPI updateCube(LPVOID lpParam);