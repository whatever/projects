#ifndef _CUBEPHORIA_H_
#define _CUBEPHORIA_H_
#pragma once

#include <vector>
#include <functional>
#include "maxcpp5.h"

namespace CubeControl
{
	class CubeController;

	enum CubeMode
	{
		/// Alternating pixels change color in a sweeping motion across
		/// the cube.
		MODE_WATERFALL = 0,

		/// A color appears on the beat and disappears quickly.
		MODE_FLASH_SHORT,

		/// Planes "slide in" from the sides and top of the cube.
		MODE_PLANES,

		/// Alternates between lighting up the whole cube and only its edges.
		MODE_WIREFRAME,

		/// Slow color fade akin to old demoscene plasmas. Best for quiet
		/// sections of songs. 
		MODE_PLASMA,

		/// Alternates between lighting up the top and bottom halves
		/// of the cube.
		MODE_HALVES,

		/// Alternates between a small, central inner cube and the full cube,
		/// changing color each time.
		MODE_SMALL_LARGE,

		/// Cube is split into four subcubes, which are each filled
		/// sequentially with random colors. When they have all been
		/// occupied, the process begins again.
		MODE_RUBIKS,

		/// A color appears on the beat and stays until the next beat.
		MODE_FLASH_LONG,

		/// Single-pixel entities ("buoys") randomly sink to the bottom of the cube or
		/// float to the top on each beat.
		MODE_SINK_FLOAT,

		/// Raindrops fall randomly, leaving fading trails behind them.
		MODE_RAIN,

		/// Beat changes the color of one face.
		MODE_FACES,

		NUM_CUBE_MODES,

		/// On each beat, a random mode is allowed to perform one action.
		MODE_CHAOS
	};

	enum Inlets
	{
		/// Bang notifies us that a beat just occurred.
		BEAT_INLET = 0,
		/// Double indicates current BPM of song.
		BPM_INLET = 1,
		/// Bang advances to the next mode. A long sets the mode number.
		MODE_INLET = 2,
		NUM_INLETS
	};

	// Clock (timer) stuff
	typedef struct _myobject
	{
		t_object m_obj;
		double m_interval;
		void *m_clock;
	} t_object;

	/// This the the Max clock that keeps our updateClock() function running.
	/// Ticks are a fixed time apart.
	t_object* gClockObject;

	class Cubephoria : public MaxCpp5<Cubephoria>
	{
	public:
		typedef std::vector<Point3> PixelBuffer;

		Cubephoria(t_symbol * sym, long ac, t_atom * av);
		~Cubephoria();

		/// Called when we get a bang on an inlet. Side effects depend upon
		/// which inlet (see the enum above).
		void bang(long whichInlet);
		void onLong(long whichInlet, long value);
		void onDouble(long whichInlet, double value);

		/// Should be called when there's a beat. Most modes
		/// are updated here.
		void notifyBeat();

		void updateTimer(float timeSinceLastCall);

		void setMode(CubeMode mode);

		void setBPM(double bpm);

		/// Advances to the next mode, or back to 0 if we're at the end.
		void nextMode();

	private:

		/// ----- BEAT UPDATE FUNCTIONS -----
		/// Depending on mode, one of these is called when there's a beat.
		/// ---------------------------------
		void _beatModePlanes();
		void _beatModeFaces();
		void _beatModeWireframe();
		void _beatModeHalves();
		void _beatModeSmallLarge();
		void _beatModeRubiks();
		void _beatModeSinkFloat();

		/// ----- TIMER UPDATE FUNCTIONS -----
		/// Called every clock tick depending on mode.
		/// ---------------------------------
		void _timerModePlanes(float timeSinceLastCall);
		void _timerModeFlashShort(float timeSinceLastCall);
		void _timerModePlasma(float timeSinceLastCall);
		void _timerModeRain(float timeSinceLastCall);
		void _timerModeWaterfall(float timeSinceLastCall);
		void _timerModeSinkFloat(float timeSinceLastCall);
		
		/// Elements will be from start to end-1, inclusive.
		std::vector<char> _getPermutationInRange(unsigned char start, unsigned char end);

		CubeControl::CubeController* mCubeController;

		CubeMode mMode;

		/// Used as a kind of "memory" for MODE_FLASH_SHORT so we can remember
		/// what color we're fading out.
		///
		/// Also serves as the color we're currently setting faces to, etc.
		CubeControl::Color3 mActiveColor;

		/// Current BPM of song (fed in through an inlet).
		/// If not received, this will be 0 and some things won't work
		/// correctly.
		double mBPM;

		/// ----- Used for MODE_PLANES.
		Point3 mLineStartPos;
		Point3 mMovementVector;
		/// -----

		/// ----- Used for MODE_SINK_FLOAT.
		/// One element for each dot (or "buoy," if you prefer).
		/// True if the pixel is sinking downward (or stuck to
		/// the bottom). False if it's rising or "pressing against"
		/// the top of the cube.
		std::vector<bool> mIsSinking;
		std::vector<Point3> mBuoyPositions;
		/// -----


	}; // end class Cubephoria

	typedef std::tr1::function<void(double)> TimerCallback;

	// The timer stuff is global because the Max SDK, being C-based, doesn't understand member functions.
	TimerCallback gTimerCallback;
	void updateClock(t_object *x);

}; // end namespace CubeControl

#endif