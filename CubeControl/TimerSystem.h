#ifndef _CUBECONTROL_TIMERSYSTEM_H_
#define _CUBECONTROL_TIMERSYSTEM_H_
#pragma once

#include <functional>
#include <map>
#include "Singleton.h"
#include <Windows.h>

namespace CubeControl
{
	typedef std::string String;
	using namespace std::tr1::placeholders;

	typedef std::tr1::function<void(double)> TimerCallback;
	typedef std::map<String, TimerCallback> TimerCallbackMap;

	class TimerSystem : public Singleton<TimerSystem>
	{
	public:
		TimerSystem();

		/// Pass in an update function. It will be called every tick.
		void registerCallback(const String& name, TimerCallback callback);

		void unregisterCallback(const String& name);

		/// Should be called every tick. Updates listeners.
		void update();

	private:
		TimerCallbackMap mCallbacks;

		/// MS performance timer frequency.
		LARGE_INTEGER mTicksPerSecond;

		/// The number of ticks we got from QueryPerformanceCounter on
		/// the previous update.
		LARGE_INTEGER mLastTicks;
	};
}

#endif