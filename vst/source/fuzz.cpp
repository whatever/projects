#include <stdlib.h>
#include "fuzz.h"
#include <string>
#include <sstream>

//-------------------------------------------------------------------------------------------------------
AudioEffect* createEffectInstance (audioMasterCallback audioMaster)
{
	return new EuphonicFuzz (audioMaster);
}

//-------------------------------------------------------------------------------------------------------
EuphonicFuzz::EuphonicFuzz (audioMasterCallback audioMaster)
: AudioEffectX (audioMaster, 1, 2)	// 1 program, 1 parameter only
{
	_initParameters();

	setNumInputs (1);		// mono in
	setNumOutputs (1);		// mono out
	setUniqueID (123456);	// identify
	canProcessReplacing ();	// supports replacing output

	mPreGain = 0.5f;
	mPostGain = 0.2f;

	mTimeOverTop = mTimeUnderBottom = 0;

	vst_strncpy (programName, "Default", kVstMaxProgNameLen);	// default program name
}

//-------------------------------------------------------------------------------------------------------
EuphonicFuzz::~EuphonicFuzz ()
{
}

void EuphonicFuzz::_initParameters()
{
	Parameter preGain;
	preGain.name = "PreGain";
	preGain.defaultValue = 0.5f;
	preGain.units = "x";
	mParameters.push_back(preGain);

	Parameter postGain;
	postGain.name = "PostGain";
	postGain.defaultValue = 1.0f;
	postGain.units = "x";
	mParameters.push_back(postGain);
}

//-------------------------------------------------------------------------------------------------------
void EuphonicFuzz::setProgramName (char* name)
{
	vst_strncpy (programName, name, kVstMaxProgNameLen);
}

//-----------------------------------------------------------------------------------------
void EuphonicFuzz::getProgramName (char* name)
{
	vst_strncpy (name, programName, kVstMaxProgNameLen);
}

//-----------------------------------------------------------------------------------------
void EuphonicFuzz::setParameter (VstInt32 index, float value)
{
	switch(index)
	{
	case 0:
		mPreGain = value;
		break;
	case 1:
		mPostGain = value;
		break;
	}
}

//-----------------------------------------------------------------------------------------
float EuphonicFuzz::getParameter (VstInt32 index)
{
	switch(index)
	{
	case 0:
		return mPreGain;
	case 1:
		return mPostGain;
	default:
		return 0;
	}
}

//-----------------------------------------------------------------------------------------
void EuphonicFuzz::getParameterName (VstInt32 index, char* label)
{
	strcpy(label, mParameters[index].name.c_str());
}

//-----------------------------------------------------------------------------------------
void EuphonicFuzz::getParameterDisplay (VstInt32 index, char* text)
{
	std::stringstream ss;
	
	switch(index)
	{
	case 0:
		ss << mPreGain;
		break;
	case 1:
		ss << mPostGain;
		break;
	}
	
	vst_strncpy(text, ss.str().c_str(), kVstMaxParamStrLen);
}

//-----------------------------------------------------------------------------------------
void EuphonicFuzz::getParameterLabel (VstInt32 index, char* label)
{
	vst_strncpy (label, mParameters[index].units.c_str(), kVstMaxParamStrLen);
}

//------------------------------------------------------------------------
bool EuphonicFuzz::getEffectName (char* name)
{
	vst_strncpy (name, "Euphonic Fuzz", kVstMaxEffectNameLen);
	return true;
}

//------------------------------------------------------------------------
bool EuphonicFuzz::getProductString (char* text)
{
	vst_strncpy (text, "Euphonic Fuzz", kVstMaxProductStrLen);
	return true;
}

//------------------------------------------------------------------------
bool EuphonicFuzz::getVendorString (char* text)
{
	vst_strncpy (text, "Jake Hart", kVstMaxVendorStrLen);
	return true;
}

//-----------------------------------------------------------------------------------------
VstInt32 EuphonicFuzz::getVendorVersion ()
{ 
	return 1000; 
}

//-----------------------------------------------------------------------------------------
void EuphonicFuzz::processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames)
{
    float* in1  =  inputs[0];
    float* out1 = outputs[0];

	while (sampleFrames >= 0)
    {
        sampleFrames--;

		const float x = (*in1); //input
		float y = 0; //output

		//--------- PRE-GAIN STAGE ----------
		y = x * mPreGain * 10;

		//--------- CLIPPING STAGE ----------
		

		//--------- POST-GAIN STAGE ----------
		(*out1) = y * mPostGain * 5;

		in1++;
		out1++;
    }	
}
