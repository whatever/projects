#include <IRremote.h>

int RECV_PIN = 2;
IRrecv irrecv(RECV_PIN);

// Remote control codes
#define IR_POWER 0x10EFD827 
#define IR_A 0x10EFF807 
#define IR_B 0x10EF7887
#define IR_C 0x10EF58A7
#define IR_UP 0x10EFA05F
#define IR_DOWN 0x10EF00FF
#define IR_LEFT 0x10EF10EF
#define IR_RIGHT 0x10EF807F
#define IR_SELECT 0x10EF20DF

// FHT stuff.
#define LOG_OUT 1 // use the log output function
#define FHT_N 256 // set to 256 point fht
#include <FHT.h>

class IRApp
{
private:
  bool m_bIsBeingControlled;

  bool m_bIsTurnedOn;
public:

  IRApp() : m_bIsBeingControlled( true ),
            m_bIsTurnedOn( true )
  {}

  void update()
  {
    pollIR();
    updateFHT();
  }
  
  void updateFHT()
  {
    cli();  // UDRE interrupt slows this way down on arduino1.0
    
    
    
    for (int i = 0 ; i < FHT_N ; i++) { // save 256 samples
      while(!(ADCSRA & 0x10)); // wait for adc to be ready
      
      ADMUX = (1<<REFS0);
 
      // reset the SRA
      // ADC Enable and prescaler of 128
      // 16000000/128 = 125000
      ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
  
      byte m = ADCL; // fetch adc data
      byte j = ADCH;
      int k = (j << 8) | m; // form into an int
      k -= 0x0200; // form into a signed int
      k <<= 6; // form into a 16b signed int
      fht_input[i] = k; // put real data into bins
    }
    fht_window(); // window the data for better frequency response
    fht_reorder(); // reorder the data before doing the fht
    fht_run(); // process the data in the fht
    fht_mag_log(); // take the output of the fht
    sei();
    Serial.write(255); // send a start byte
    Serial.write(fht_log_out, FHT_N/2); // send out the data
  }
  
  void pollIR()
  {
    decode_results results;
    
    if( irrecv.decode(&results) )
      irrecv.resume();
    
    if( results.value != IR_A && !m_bIsBeingControlled )
    {
      return;
    }
    
    if( results.value != IR_POWER && !m_bIsTurnedOn )
    {
      return;
    }
    
    switch( results.value )
    {
      case IR_POWER:
      {
        if( m_bIsBeingControlled )
          m_bIsTurnedOn = !m_bIsTurnedOn;
        break;
      }
      case IR_A:
      {
        Serial.println("ME");
        m_bIsBeingControlled = true;
        break;
      }
      case IR_B:
      case IR_C: 
      {
        Serial.println("OTHER");
        m_bIsBeingControlled = false;
        break;
      }
      case IR_UP:
      {
        Serial.println("UP");
        break;
      }
      case IR_DOWN:
      {
        Serial.println("DOWN");
        break;
      }
      case IR_LEFT:
      {
        Serial.println("LEFT");
        break;
      }
      case IR_RIGHT:
      {
        Serial.println("RIGHT");
        break;
      }
      case IR_SELECT:
      {
        Serial.println("SELECT");
        break;
      }
    }
  }
};

void setup()
{
  Serial.begin(115200);
  pinMode(9, OUTPUT);
  irrecv.enableIRIn(); // Start the receiver
  
  
}

IRApp irApp;
void loop() 
{
  while( 1 ) // reduce jitter
  {
    irApp.update();
  }
}
