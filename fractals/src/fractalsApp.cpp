#include "cinder/app/AppBasic.h"
#include "cinder/gl/gl.h"

#include "cinder/TriMesh.h"
#include "cinder/Camera.h"
#include "cinder/ImageIo.h"
#include "cinder/gl/Light.h"
#include "cinder/gl/Texture.h"
#include "cinder/params/Params.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class fractalsApp : public AppBasic {
  public:
	void setup();
	void mouseDown( MouseEvent event );	
	void update();
	void draw();
private:
	void AppendPyramid( TriMesh& mesh, const Vec3f& origin, float width );
	// For isocahedrons
	float GetPyramidHeight( float width );
	void CreateSierpinski( TriMesh& mesh, const Vec3f& origin, float width, size_t numIterations );
	Vec3f CalculateNormal(const Vec3f& v0, const Vec3f& v1, const Vec3f& v2);
	Vec3f CalculateNormal( const TriMesh& mesh, size_t triIndex );
	void prepareSettings( Settings *settings );
	void keyDown( KeyEvent event );

	params::InterfaceGl mParams;
	
	CameraPersp mCam;
	TriMesh mMesh;
	gl::Texture mTexture;
	gl::Light* mLight;

	Vec3f mCamPos;
	Vec3f mCenter;
	Vec3f mUp;
	Quatf mCamOrientation;
	float mCamX;
	float mCamY;
	float mCamZ;
	double mMoveSpeed;
};

float fractalsApp::GetPyramidHeight( float width )
{
	return sqrt( pow( width, 2 ) - pow( width/2, 2 ) );
}

void fractalsApp::prepareSettings( Settings *settings )
{
	settings->setWindowSize( 800, 600 );
	settings->setFrameRate( 60.0f );
}

void fractalsApp::setup()
{
	mCamPos = Vec3f( 0, 150.f, 0);
	mCenter = Vec3f::zero();
	mUp = Vec3f::yAxis();
	mCam.setPerspective( 45.0f, getWindowAspectRatio(), 5.0f, 10000.0f );

	mMoveSpeed = 5.0f;

	CreateSierpinski( mMesh, Vec3f(0,0,0), 1000.f, 7 );
	mMesh.recalculateNormals();

	glEnable( GL_LIGHTING );
	mLight = new gl::Light( gl::Light::DIRECTIONAL, 0 );
	mLight->setPosition( Vec3f::zero() );
	mLight->setDiffuse(Color(1,1,1));
	mLight->setDirection(Vec3f::zAxis());
	mLight->enable();

	Url url( "http://libcinder.org/docs/v0.8.4/tutorial_part1_01.jpg" );
	mTexture = gl::Texture( loadImage( loadUrl( url ) ) );

	glShadeModel( GL_SMOOTH );

	gl::enableDepthRead();
	gl::enableDepthWrite();

	mParams = params::InterfaceGl( "Fractals", Vec2i( 225, 150 ) );
	mParams.addParam( "Scene Rotation", &mCamOrientation );
	/*mParams.addParam( "Cam X", &mCamX, "min=50.0 max=5000.0 step=50.0 keyIncr=a keyDecr=d" );
	mParams.addParam( "Cam Y", &mCamY, "min=50.0 max=5000.0 step=50.0 keyIncr=e keyDecr=q" );
	mParams.addParam( "Cam Z", &mCamZ, "min=50.0 max=5000.0 step=50.0 keyIncr=s keyDecr=w" );*/
}

void fractalsApp::CreateSierpinski( TriMesh& mesh, const Vec3f& origin, float width, size_t numIterations )
{
	if( numIterations == 1 )
	{
		AppendPyramid( mesh, origin, width);
	}
	else
	{
		float halfWidth = width / 2.f;
		float quarterWidth = width / 4.f;

		// four bottom pyramids
		CreateSierpinski( mesh, Vec3f( -quarterWidth + origin.x, origin.y, -quarterWidth + origin.z ), halfWidth, numIterations - 1 );
		CreateSierpinski( mesh, Vec3f( -quarterWidth + origin.x, origin.y, quarterWidth + origin.z ), halfWidth, numIterations - 1 );
		CreateSierpinski( mesh, Vec3f( quarterWidth + origin.x, origin.y, -quarterWidth + origin.z ), halfWidth, numIterations - 1 );
		CreateSierpinski( mesh, Vec3f( quarterWidth + origin.x, origin.y, quarterWidth + origin.z ), halfWidth, numIterations - 1 );

		// top pyramid
		CreateSierpinski( mesh, Vec3f( origin.x, GetPyramidHeight(width / 2.f) + origin.y, origin.z ), width / 2.f, numIterations - 1 );
	}
}

void fractalsApp::AppendPyramid( TriMesh& mesh, const Vec3f& origin, float width )
{
	float halfWidth = width / 2.f;
	float height = GetPyramidHeight( width );

	// Vertices for the four corners of the base
	mesh.appendVertex( Vec3f( origin.x - halfWidth, origin.y, origin.z - halfWidth ) );
	mesh.appendColorRgb( Color(0.5, 0.5, 0.5 ) );
	mesh.appendTexCoord( Vec2f(0,0) );
	mesh.appendVertex( Vec3f( origin.x - halfWidth, origin.y, origin.z + halfWidth ) );
	mesh.appendColorRgb( Color(0.5, 0.5, 0.5 ) );
	mesh.appendTexCoord( Vec2f(0,1) );
	mesh.appendVertex( Vec3f( origin.x + halfWidth, origin.y, origin.z + halfWidth ) );
	mesh.appendColorRgb( Color(0.5, 0.5, 0.5 ) );
	mesh.appendTexCoord( Vec2f(1,1) );
	mesh.appendVertex( Vec3f( origin.x + halfWidth, origin.y, origin.z - halfWidth ) );
	mesh.appendColorRgb( Color(0.5, 0.5, 0.5 ) );
	mesh.appendTexCoord( Vec2f(1,0) );

	// Vertex for pointy bit
	float pointY = height + origin.y;
	mesh.appendVertex( Vec3f( origin.x, pointY, origin.z ) );
	mesh.appendColorRgb( Color(0.5, 0.5, 0.5 ) );
	mesh.appendTexCoord( Vec2f(0.5,0) );

	// Indices for each of the four corners of the base
	int vIdx0 = mesh.getNumVertices() - 5;
	int vIdx1 = mesh.getNumVertices() - 4;
	int vIdx2 = mesh.getNumVertices() - 3;
	int vIdx3 = mesh.getNumVertices() - 2;

	// Index for top point
	int vIdx4 = mesh.getNumVertices() - 1;

	// Two triangles to create our base
	mesh.appendTriangle( vIdx0, vIdx1, vIdx2 );
	mesh.appendTriangle( vIdx0, vIdx2, vIdx3 );

	// Four triangles for the faces
	mesh.appendTriangle( vIdx0, vIdx1, vIdx4 );
	mesh.appendTriangle( vIdx1, vIdx2, vIdx4 );
	mesh.appendTriangle( vIdx2, vIdx3, vIdx4 );
	mesh.appendTriangle( vIdx3, vIdx0, vIdx4 );
}

Vec3f fractalsApp::CalculateNormal(const Vec3f& v0, const Vec3f& v1, const Vec3f& v2)
{
	Vec3f e0 = v1 - v0;
	Vec3f e1 = v2 - v0;
	return e0.cross(e1).normalized();
}

Vec3f fractalsApp::CalculateNormal( const TriMesh& mesh, size_t triIndex )
{
	Vec3f v0 = Vec3f::zero();
	Vec3f v1 = Vec3f::zero();
	Vec3f v2 = Vec3f::zero();

	mesh.getTriangleVertices( triIndex, &v0, &v1, &v2 );

	return CalculateNormal( v0, v1, v2 );
}

void fractalsApp::mouseDown( MouseEvent event )
{
}

void fractalsApp::keyDown( KeyEvent event )
{
	Vec3f direction;

	switch( event.getChar() )
	{
	case 'w':
		direction = -Vec3f::zAxis();
		break;
	case 's':
		direction = Vec3f::zAxis();
		break;
	case 'a':
		direction = -Vec3f::xAxis();
		break;
	case 'd':
		direction = Vec3f::xAxis();
		break;
	case 'e':
		direction = Vec3f::yAxis();
		break;
	case 'q':
		direction = -Vec3f::yAxis();
		break;
	default:
		return;
	}

	mCamPos += mCamOrientation * direction * mMoveSpeed;
}

void fractalsApp::update()
{
	//mCamPos = mCamOrientation * Vec3f( mCamX, mCamY, mCamZ );
	mCam.lookAt( mCamPos, mCenter, mUp );
	mCam.setOrientation(mCamOrientation);

	mLight->update( mCam );
}

void fractalsApp::draw()
{
	gl::clear( Color( 0.39f, 0.58f, 0.93f ) );

	mTexture.enableAndBind();
	gl::draw( mMesh );
	mTexture.unbind();
	
	mParams.draw();

	gl::setMatrices( mCam );
	//gl::rotate( mSceneRotation );
}

CINDER_APP_BASIC( fractalsApp, RendererGl )