#undef UNICODE
#include "CubeControl.h"
#include <windows.h>
#include <iostream>
#include <algorithm>

using namespace std;

namespace CubeControl
{
	const Point3 Point3::ZERO(0, 0, 0);

	Color3 HSL2RGB(double h, double sl, double l)
	{
		double v;
		double r,g,b;

		r = l;   // default to gray
		g = l;
		b = l;
		v = (l <= 0.5) ? (l * (1.0 + sl)) : (l + sl - l * sl);
		if (v > 0)
		{
			double m;
			double sv;
			int sextant;
			double fract, vsf, mid1, mid2;

			m = l + l - v;
			sv = (v - m ) / v;
			h *= 6.0;
			sextant = (int)h;
			fract = h - sextant;
			vsf = v * sv * fract;
			mid1 = m + vsf;
			mid2 = v - vsf;
			switch (sextant)
			{
			case 0:
				r = v;
				g = mid1;
				b = m;
				break;
			case 1:
				r = mid2;
				g = v;
				b = m;
				break;
			case 2:
				r = m;
				g = v;
				b = mid1;
				break;
			case 3:
				r = m;
				g = mid2;
				b = v;
				break;
			case 4:
				r = mid1;
				g = m;
				b = v;
				break;
			case 5:
				r = v;
				g = m;
				b = mid2;
				break;
			}
		}
		Color3 rgb;
		rgb.x = (BYTE)(r * 255.0f);
		rgb.y = (BYTE)(g * 255.0f);
		rgb.z = (BYTE)(b * 255.0f);
		return rgb;
	}

	float3 RGB2HSL(double r, double g, double b)
	{
		r = r/255.0;
		g = g/255.0;
		b = b/255.0;
		double v;
		double m;
		double vm;
		double r2, g2, b2;

		double h = 0; // default to black
		double s = 0;
		double l = 0;
		v = max(r,g);
		v = max(v,b);
		m = min(r,g);
		m = min(m,b);
		l = (m + v) / 2.0;
		if (l <= 0.0)
		{
			return float3(h, s, l);
		}
		vm = v - m;
		s = vm;
		if (s > 0.0)
		{
			s /= (l <= 0.5) ? (v + m ) : (2.0 - v - m) ;
		}
		else
		{
			return float3(h, s, l);
		}
		r2 = (v - r) / vm;
		g2 = (v - g) / vm;
		b2 = (v - b) / vm;
		if (r == v)
		{
			h = (g == m ? 5.0 + b2 : 1.0 - g2);
		}
		else if (g == v)
		{
			h = (b == m ? 1.0 + r2 : 3.0 - b2);
		}
		else
		{
			h = (r == m ? 3.0 + g2 : 5.0 - r2);
		}
		h /= 6.0;

		return float3(h, s, l);
	}
	
	CubeController::CubeController(const String& portName) : mSerialPort(NULL), mBytesWritten(0)
	{
		_initSerialPort(portName);
	}
	
	CubeController::~CubeController()
	{
		clearToColor(0,0,0);
		Sleep(50);
		_closeSerialPort();
	}

	Point2 CubeController::to2DCoords(const Point3& coords)
	{
		Point2 retVal;

		bool rightSide = coords.x > 1;

		retVal.y = coords.y;
		if(rightSide)
			retVal.y += 4;

		retVal.x = coords.z;
		if(coords.x == 0 || coords.x == 2)
			retVal.x += (3-retVal.x) * 2 + 1;

		return retVal;
	}

	void CubeController::clearToColor(unsigned char r, unsigned char g, unsigned char b)
	{
		unsigned char buffer[8] = {114, 8, 0, 0, 0, r, g, b};
		WriteFile(mSerialPort, buffer, 8, &mBytesWritten, NULL);
	}

	void CubeController::clearToColor(const Color3& color)
	{
		clearToColor(color.x, color.y, color.z);
	}

	void CubeController::setPixel(const Point3& pos, const Color3& color)
	{
		Point2 coords = to2DCoords(Point3(pos.x, pos.y, pos.z));
		unsigned char buffer[8] = {114, 11, coords.x, coords.y, 0, color.x, color.y, color.z};
		WriteFile(mSerialPort, buffer, 8, &mBytesWritten, NULL);
	}

	void CubeController::swapBuffers()
	{
		unsigned char buffer[5] = {82, 31, 0, 0, 0};
		WriteFile(mSerialPort, buffer, 5, &mBytesWritten, NULL);
	}

	void CubeController::drawCube(const Point3& startPos, BYTE size, const Point3& color)
	{
		drawCuboid(startPos, startPos + (size - 1), color);
	}

	void CubeController::drawCuboid(const Point3& startPos, const Point3& endPos, const Point3& color)
	{
		for(BYTE x = startPos.x; x <= endPos.x; ++ x)
			for(BYTE y = startPos.y; y <= endPos.y; ++ y)
				for(BYTE z = startPos.z; z <= endPos.z; ++ z)
				{
					setPixel(Point3(x, y, z), color);
				}
	}

	void CubeController::setColumn(BYTE x, BYTE z, BYTE height, const Point3& color)
	{
		for(BYTE y = 0; y < height; ++y)
		{
			setPixel(Point3(x, y, z), color);
		}
	}

	void CubeController::setFace(CubeControl::Face face, const Point3& color)
	{
		switch(face)
		{
		case FACE_FRONT:
			drawCuboid(Point3(0, 0, CUBE_SIZE - 1), Point3(CUBE_SIZE - 1, CUBE_SIZE - 1, CUBE_SIZE - 1), color);
			break;
		case FACE_BACK:
			drawCuboid(Point3(0, 0, 0), Point3(CUBE_SIZE - 1, CUBE_SIZE - 1, 0), color);
			break;
		case FACE_LEFT:
			drawCuboid(Point3(0, 0, 0), Point3(0, CUBE_SIZE - 1, CUBE_SIZE - 1), color);
			break;
		case FACE_RIGHT:
			drawCuboid(Point3(CUBE_SIZE - 1, 0, 0), Point3(CUBE_SIZE - 1, CUBE_SIZE - 1, CUBE_SIZE - 1), color);
			break;
		case FACE_TOP:
			drawCuboid(Point3(0, CUBE_SIZE - 1, 0), Point3(CUBE_SIZE - 1, CUBE_SIZE - 1, CUBE_SIZE - 1), color);
			break;
		case FACE_BOTTOM:
			drawCuboid(Point3(0, 0, 0), Point3(CUBE_SIZE - 1, 0, CUBE_SIZE - 1), color);
			break;
		case FACE_INSIDE:
			drawCuboid(Point3(1, 1, 1), Point3(CUBE_SIZE - 2, CUBE_SIZE - 2, CUBE_SIZE - 2), color);
		default:
			// Should never get here.
			break;
		}
	}
	
	void CubeController::drawLine(const Point3& start, const Point3& end, const Color3& color)
	{
		int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc, err_1, err_2, dx2, dy2, dz2;
		Point3 pixel(0,0,0);

		pixel.x = start.x;
		pixel.y = start.y;
		pixel.z = start.z;
		dx = end.x - start.x;
		dy = end.y - start.y;
		dz = end.z - start.z;
		x_inc = (dx < 0) ? -1 : 1;
		l = abs(dx);
		y_inc = (dy < 0) ? -1 : 1;
		m = abs(dy);
		z_inc = (dz < 0) ? -1 : 1;
		n = abs(dz);
		dx2 = l << 1;
		dy2 = m << 1;
		dz2 = n << 1;

		if ((l >= m) && (l >= n)) {
			err_1 = dy2 - l;
			err_2 = dz2 - l;
			for (i = 0; i < l; i++) {
				setPixel(pixel, color);
				if (err_1 > 0) {
					pixel.y += y_inc;
					err_1 -= dx2;
				}
				if (err_2 > 0) {
					pixel.z += z_inc;
					err_2 -= dx2;
				}
				err_1 += dy2;
				err_2 += dz2;
				pixel.x += x_inc;
			}
		} else if ((m >= l) && (m >= n)) {
			err_1 = dx2 - m;
			err_2 = dz2 - m;
			for (i = 0; i < m; i++) {
				setPixel(pixel, color);
				if (err_1 > 0) {
					pixel.x += x_inc;
					err_1 -= dy2;
				}
				if (err_2 > 0) {
					pixel.z += z_inc;
					err_2 -= dy2;
				}
				err_1 += dx2;
				err_2 += dz2;
				pixel.y += y_inc;
			}
		} else {
			err_1 = dy2 - n;
			err_2 = dx2 - n;
			for (i = 0; i < n; i++) {
				setPixel(pixel, color);
				if (err_1 > 0) {
					pixel.y += y_inc;
					err_1 -= dz2;
				}
				if (err_2 > 0) {
					pixel.x += x_inc;
					err_2 -= dz2;
				}
				err_1 += dy2;
				err_2 += dx2;
				pixel.z += z_inc;
			}
		}
		setPixel(pixel, color);
	}

	void CubeController::scroll()
	{
		unsigned char buffer[5] = {82, 11, 0, 0, 0};
		WriteFile(mSerialPort, buffer, 8, &mBytesWritten, NULL);
	}

	void CubeController::_initSerialPort(const String& portName)
	{
		// Convert the port name string into a wstring
		std::wstring wPortName(portName.length(), L' ');
		std::copy(portName.begin(), portName.end(), wPortName.begin());

		mSerialPort = CreateFile(portName.c_str(),
			GENERIC_WRITE,
			0,
			0,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			0);

		if(mSerialPort == INVALID_HANDLE_VALUE)
		{
			if(GetLastError() == ERROR_FILE_NOT_FOUND)
				printf("serial port does not exist.\n");
			else
				printf("some other error occurred.\n");
		}


		// set some parameters
		DCB dcbSerialParams = {0};
		dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
		if (!GetCommState(mSerialPort, &dcbSerialParams))
			printf("error getting state\n");

		dcbSerialParams.BaudRate = CBR_115200;
		dcbSerialParams.ByteSize = 8;
		dcbSerialParams.StopBits = ONESTOPBIT;
		dcbSerialParams.Parity = NOPARITY;
		if(!SetCommState(mSerialPort, &dcbSerialParams)){
			printf("error setting serial port state\n");
		}

		COMMTIMEOUTS timeouts={0};
		timeouts.ReadIntervalTimeout=INFINITE;
		timeouts.ReadTotalTimeoutConstant=INFINITE;
		timeouts.ReadTotalTimeoutMultiplier=10;
		timeouts.WriteTotalTimeoutConstant=INFINITE;
		timeouts.WriteTotalTimeoutMultiplier=10;
		if(!SetCommTimeouts(mSerialPort, &timeouts)){
			printf("could not set timeouts\n");
		}
	}

	void CubeController::_closeSerialPort()
	{
		CloseHandle(mSerialPort);
	}
};